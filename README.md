# Current Measurement and Analsysis of Digital Circuits and Systems

So here is the top level README. Ideally everything here will get you going. We did not move the needle as much as we had hoped, and in fact what appeared to be the modest goal of simply performing a successful DPA slipped through our grasp. Carlo and I have put this repo together to collect in one organized and easy place all of our work and the prior group's work that we have used. 

There may be some funny business even if you are familiar with git if you get to the point of storing synthesis files up here. Git was developed with taditional software workflows in mind and with a little effort we have been able to store Aldec workspaces up here, but recall that typically we use Aldec for simulation files and unless you are using it as  front-end to quartus thats all it will be used for. 

I'm sure hardware design stuff can be stored up here, but it will take a little figuring out. I intended (with more time) to figure out how to store the designs up here with the appropriate build scripts. But like any other repo, the idea is to find out what the minimum stuff you can store is, and add all the generated stuff to the gitignore. I have not yet figured out what these are for quartus although there are some suggestions [here](https://github.com/thomasrussellmurphy/quartus-DE1_SOC-project/blob/master/.gitignore) and [here](http://gitignore.io/api/alteraquartusii). The only thing these seem not to include is the host of files associated with the NIOS II (although interestingly one does include the stuff you would need to use the ARM cores of the HPS on a cyclone SoC package).

If you need help configuring anything or have questions about the project in general you should be able to reach me, Micah, here: mwl2306@gmail.com.

Good Luck

***

P.S. Don't forget to the check out the [wiki](https://gitlab.com/mwl2306/ece_487_fixed/wikis/home)

*** local repo locataed at C:\Users\engadmin\Desktop\ece_487_fixed ***

*** P.P.S [Here's a link](https://gitlab.com/mwl2306/aes128_nios2) to the NIOS II repo, explanation therein***
