# QUARTUS README

Currently we have not yet made it to quartus and sadly it looks as though we may not. However I will attempt to move the prior groups 32 bit AES proof of concept stuff here if possible. 

Micah Lehman 11/17/2019


design 32 bit is the inherited, simplified 32-bit design.  

AES128_simple is Carlo's compact 128 bit design.  

This folder previously contained an early version of the AES 128 with a Nios 2 soft core added. This has been moved to a separate repo as I am unsure how to add the stuff from NIOS II software build toos to version control. You can find the repo [here](https://gitlab.com/mwl2306/aes128_nios2).
