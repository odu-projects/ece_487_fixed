library ieee;
use ieee.numeric_std.all;
use ieee.std_logic_1164.all; 

entity Simplified_AES is
	port (clk_50, rst, st: in std_logic; seed: in std_logic_vector(7 downto 0);
	      done, trigger, LEDG0: out std_logic; hex0, hex1, hex2, hex3, hex4, 
		  hex5, hex6, hex7: out std_logic_vector(6 downto 0)); 
end Simplified_AES;

-- DE2-115 Media Board Configuration:
   ---------------------------------
-- clk = PIN_Y2 = CLOCK_50
-- done  = PIN_H15 = LEDR17
-- trigger = PIN_AG26 pin 40, GPIO 35 (see manual)
-- LEDG0 = PIN_E21
-- rst = reset = pushbutton 1 (key1 = PIN_M21)
-- seed = vector(7 downto 0) = SW7 downto SW0 = PIN_AB26, PIN_AD26, PIN_AC26, PIN_AB27
--                                              PIN_AD27, PIN_AC27, PIN_AC28, PIN_AB28
-- st = start = pushbutton 0 (key0 = PIN_M23)
-- TxD = Serial port transmitter = UART_TXD = PIN_G9


architecture dataflow_encrypt of Simplified_AES is	

--component AES_clkDIV IS --100Khz clock
--	PORT
--	(
--		areset		: IN STD_LOGIC  ;
--		inclk0		: IN STD_LOGIC  ;
--		c0				: OUT STD_LOGIC ;
--		locked		: OUT STD_LOGIC 
--	);
--END component;

component pll IS		-- trying different clock speeds 50MHz, 5MHz, 1MHz, 100KHz from c0-c3
	PORT
	(
		areset		: IN STD_LOGIC  := '0';
		inclk0		: IN STD_LOGIC  := '0';
		c0		: OUT STD_LOGIC ;
		c1		: OUT STD_LOGIC ;
		c2		: OUT STD_LOGIC ;
		c3		: OUT STD_LOGIC 
	);
END component;

type Table16Bx16B is array (0 to 15, 0 to 15) of unsigned(7 downto 0);
type Array2Bx2B is array (0 to 1, 0 to 1) of unsigned(7 downto 0);

signal plainText: std_logic_vector(31 downto 0);
signal key: std_logic_vector(31 downto 0);
signal q: std_logic_vector(31 downto 0);
signal plainText_out: std_logic_vector(39 downto 0);
signal cipherText: std_logic_vector(31 downto 0);
signal u_cipherText: unsigned(31 downto 0);

signal state, nextstate: integer := 0;

signal i: integer := 0;
signal j: integer := 0;
signal m: integer := 0;
signal n: integer := 0;
signal x: integer := 0;
signal y: integer := 0;
signal adv_i, rst_i, adv_j, rst_j: std_logic := '0';
signal adv_m, rst_m, adv_n, rst_n: std_logic := '0';
signal adv_x, rst_x, adv_y, rst_y: std_logic := '0';

signal st_debounced: std_logic;
signal rst_debounced: std_logic;

signal initAll: std_logic;
signal load_done: std_logic;
signal load_trigger: std_logic;
signal load_q: std_logic;
signal load_plainText: std_logic;
signal load_plainText_out: std_logic;
signal load_ledg0: std_logic;
signal load_ptBlock_k: std_logic;
signal load_b_block: std_logic;
signal load_sBox_block: std_logic;
signal load_rot_block: std_logic;
signal load_mc_block: std_logic;
signal load_xor_block: std_logic;
signal load_u_cipher: std_logic;
signal load_cipher: std_logic;
signal load_TxD: std_logic;

signal ptBlock: Array2Bx2B;
signal b: Array2Bx2B;
signal k: Array2Bx2B;

signal clk, clock_50MHz, clock_5MHz, clock_1MHz, clock_100KHz: std_logic;

constant sBox: Table16Bx16B :=
		((x"63", x"7c", x"77", x"7b", x"f2", x"6b", x"6f", x"c5",
		  x"30", x"01", x"67", x"2b", x"fe", x"d7", x"ab", x"76"), 
		(x"ca", x"82", x"c9", x"7d", x"fa", x"59", x"47", x"f0",
         x"ad", x"d4", x"a2", x"af", x"9c", x"a4", x"72", x"c0"),
		(x"b7", x"fd", x"93", x"26", x"36", x"3f", x"f7", x"cc",
		 x"34", x"a5", x"e5", x"f1", x"71", x"d8", x"31", x"15"),
		(x"04", x"c7", x"23", x"c3", x"18", x"96", x"05", x"9a",
		 x"07", x"12", x"80", x"e2", x"eb", x"27", x"b2", x"75"),
		(x"09", x"83", x"2c", x"1a", x"1b", x"6e", x"5a", x"a0",
		 x"52", x"3b", x"d6", x"b3", x"29", x"e3", x"2f", x"84"),
		(x"53", x"d1", x"00", x"ed", x"20", x"fc", x"b1", x"5b",
		 x"6a", x"cb", x"be", x"39", x"4a", x"4c", x"58", x"cf"),
		(x"d0", x"ef", x"aa", x"fb", x"43", x"4d", x"33", x"85",
		 x"45", x"f9", x"02", x"7f", x"50", x"3c", x"9f", x"a8"),
		(x"51", x"a3", x"40", x"8f", x"92", x"9d", x"38", x"f5",
		 x"bc", x"b6", x"da", x"21", x"10", x"ff", x"f3", x"d2"),
		(x"cd", x"0c", x"13", x"ec", x"5f", x"97", x"44", x"17",
		 x"c4", x"a7", x"7e", x"3d", x"64", x"5d", x"19", x"73"),
		(x"60", x"81", x"4f", x"dc", x"22", x"2a", x"90", x"88",
		 x"46", x"ee", x"b8", x"14", x"de", x"5e", x"0b", x"db"),
		(x"e0", x"32", x"3a", x"0a", x"49", x"06", x"24", x"5c",
		 x"c2", x"d3", x"ac", x"62", x"91", x"95", x"e4", x"79"),
		(x"e7", x"c8", x"37", x"6d", x"8d", x"d5", x"4e", x"a9",
		 x"6c", x"56", x"f4", x"ea", x"65", x"7a", x"ae", x"08"),
		(x"ba", x"78", x"25", x"2e", x"1c", x"a6", x"b4", x"c6",
		 x"e8", x"dd", x"74", x"1f", x"4b", x"bd", x"8b", x"8a"),
		(x"70", x"3e", x"b5", x"66", x"48", x"03", x"f6", x"0e",
		 x"61", x"35", x"57", x"b9", x"86", x"c1", x"1d", x"9e"),
		(x"e1", x"f8", x"98", x"11", x"69", x"d9", x"8e", x"94",
		 x"9b", x"1e", x"87", x"e9", x"ce", x"55", x"28", x"df"),
		(x"8c", x"a1", x"89", x"0d", x"bf", x"e6", x"42", x"68",
		 x"41", x"99", x"2d", x"0f", x"b0", x"54", x"bb", x"16"));

constant mc_x_2: Table16Bx16B :=
		((x"00", x"02", x"04", x"06", x"08", x"0a", x"0c", x"0e",
		  x"10", x"12", x"14", x"16", x"18", x"1a", x"1c", x"1e"), 
		(x"20", x"22", x"24", x"26", x"28", x"2a", x"2c", x"2e",
		 x"30", x"32", x"34", x"36", x"38", x"3a", x"3c", x"3e"), 
		(x"40", x"42", x"44", x"46", x"48", x"4a", x"4c", x"4e",
		 x"50", x"52", x"54", x"56", x"58", x"5a", x"5c", x"5e"), 
		(x"60", x"62", x"64", x"66", x"68", x"6a", x"6c", x"6e",
		 x"70", x"72", x"74", x"76", x"78", x"7a", x"7c", x"7e"), 
		(x"80", x"82", x"84", x"86", x"88", x"8a", x"8c", x"8e",
		 x"90", x"92", x"94", x"96", x"98", x"9a", x"9c", x"9e"), 
		(x"a0", x"a2", x"a4", x"a6", x"a8", x"aa", x"ac", x"ae",
		 x"b0", x"b2", x"b4", x"b6", x"b8", x"ba", x"bc", x"be"), 
		(x"c0", x"c2", x"c4", x"c6", x"c8", x"ca", x"cc", x"ce",
		 x"d0", x"d2", x"d4", x"d6", x"d8", x"da", x"dc", x"de"), 
		(x"e0", x"e2", x"e4", x"e6", x"e8", x"ea", x"ec", x"ee",
		 x"f0", x"f2", x"f4", x"f6", x"f8", x"fa", x"fc", x"fe"), 
		(x"1b", x"19", x"1f", x"1d", x"13", x"11", x"17", x"15",
		 x"0b", x"09", x"0f", x"0d", x"03", x"01", x"07", x"05"), 
		(x"3b", x"39", x"3f", x"3d", x"33", x"31", x"37", x"35",
		 x"2b", x"29", x"2f", x"2d", x"23", x"21", x"27", x"25"), 
		(x"5b", x"59", x"5f", x"5d", x"53", x"51", x"57", x"55",
		 x"4b", x"49", x"4f", x"4d", x"43", x"41", x"47", x"45"), 
		(x"7b", x"79", x"7f", x"7d", x"73", x"71", x"77", x"75",
		 x"6b", x"69", x"6f", x"6d", x"63", x"61", x"67", x"65"), 
		(x"9b", x"99", x"9f", x"9d", x"93", x"91", x"97", x"95",
		 x"8b", x"89", x"8f", x"8d", x"83", x"81", x"87", x"85"), 
		(x"bb", x"b9", x"bf", x"bd", x"b3", x"b1", x"b7", x"b5",
		 x"ab", x"a9", x"af", x"ad", x"a3", x"a1", x"a7", x"a5"), 
		(x"db", x"d9", x"df", x"dd", x"d3", x"d1", x"d7", x"d5",
		 x"cb", x"c9", x"cf", x"cd", x"c3", x"c1", x"c7", x"c5"), 
		(x"fb", x"f9", x"ff", x"fd", x"f3", x"f1", x"f7", x"f5",
		 x"eb", x"e9", x"ef", x"ed", x"e3", x"e1", x"e7", x"e5"));	

constant mc_x_3: Table16Bx16B :=	
		((x"00", x"03", x"06", x"05", x"0c", x"0f", x"0a", x"09",
		  x"18", x"1b", x"1e", x"1d", x"14", x"17", x"12", x"11"), 
		(x"30", x"33", x"36", x"35", x"3c", x"3f", x"3a", x"39",
		 x"28", x"2b", x"2e", x"2d", x"24", x"27", x"22", x"21"), 
		(x"60", x"63", x"66", x"65", x"6c", x"6f", x"6a", x"69",
		 x"78", x"7b", x"7e", x"7d", x"74", x"77", x"72", x"71"), 
		(x"50", x"53", x"56", x"55", x"5c", x"5f", x"5a", x"59",
		 x"48", x"4b", x"4e", x"4d", x"44", x"47", x"42", x"41"), 
		(x"c0", x"c3", x"c6", x"c5", x"cc", x"cf", x"ca", x"c9",
		 x"d8", x"db", x"de", x"dd", x"d4", x"d7", x"d2", x"d1"), 
		(x"f0", x"f3", x"f6", x"f5", x"fc", x"ff", x"fa", x"f9",
		 x"e8", x"eb", x"ee", x"ed", x"e4", x"e7", x"e2", x"e1"), 
		(x"a0", x"a3", x"a6", x"a5", x"ac", x"af", x"aa", x"a9",
		 x"b8", x"bb", x"be", x"bd", x"b4", x"b7", x"b2", x"b1"), 
		(x"90", x"93", x"96", x"95", x"9c", x"9f", x"9a", x"99",
		 x"88", x"8b", x"8e", x"8d", x"84", x"87", x"82", x"81"), 
		(x"9b", x"98", x"9d", x"9e", x"97", x"94", x"91", x"92",
		 x"83", x"80", x"85", x"86", x"8f", x"8c", x"89", x"8a"), 
		(x"ab", x"a8", x"ad", x"ae", x"a7", x"a4", x"a1", x"a2",
		 x"b3", x"b0", x"b5", x"b6", x"bf", x"bc", x"b9", x"ba"), 
		(x"fb", x"f8", x"fd", x"fe", x"f7", x"f4", x"f1", x"f2",
		 x"e3", x"e0", x"e5", x"e6", x"ef", x"ec", x"e9", x"ea"), 
		(x"cb", x"c8", x"cd", x"ce", x"c7", x"c4", x"c1", x"c2",
		 x"d3", x"d0", x"d5", x"d6", x"df", x"dc", x"d9", x"da"), 
		(x"5b", x"58", x"5d", x"5e", x"57", x"54", x"51", x"52",
		 x"43", x"40", x"45", x"46", x"4f", x"4c", x"49", x"4a"), 
		(x"6b", x"68", x"6d", x"6e", x"67", x"64", x"61", x"62",
		 x"73", x"70", x"75", x"76", x"7f", x"7c", x"79", x"7a"), 
		(x"3b", x"38", x"3d", x"3e", x"37", x"34", x"31", x"32",
		 x"23", x"20", x"25", x"26", x"2f", x"2c", x"29", x"2a"), 
		(x"0b", x"08", x"0d", x"0e", x"07", x"04", x"01", x"02",
		 x"13", x"10", x"15", x"16", x"1f", x"1c", x"19", x"1a"));
		 

begin
	
--	pll: AES_clkDIV
--		port map ( 	areset 	=> '0',
--						inclk0 	=> clk_50,
--						c0			=> clk,
--						locked	=> open
--					);
				
	pll1: pll     -- trying different clock speeds 50MHz, 5MHz, 1MHz, 100KHz from c0-c3
	PORT map
	(
		areset		=> '0',
		inclk0		=> clk_50,
		c0				=> clock_50MHz,
		c1				=> clock_5MHz,
		c2				=> clock_1MHz,
		c3				=> clk
	);					

	process(clk)
	variable s1_st, s2_st: std_logic;
	variable s1_rst, s2_rst: std_logic;
	begin
		if clk'event and clk='1' then
			s2_st := s1_st;
			s1_st := st;
			s2_rst := s1_rst;
			s1_rst := rst;
		end if;
		st_debounced <= s2_st;
		rst_debounced <= s2_rst;
	end process;
	
	-- state change ------------------------------------------------------------	
	process(clk,rst_debounced)
	begin
		if rst_debounced = '0' then
		   state <= 0;
		elsif clk'event and clk = '1' then
			state <= nextState;
		end if;
	end process;

-- Counter i ----Encryption iterations--------------------------------------
	process(clk)
	begin
		if clk'event and clk = '1' then
			if rst_i = '1' then
				i <= 0;
			elsif adv_i = '1' then
				i <= i + 1;
			end if;
		end if;
	end process;
	
-- Counter j ----Baud rate conversion---------------------------------------
--	process(clk)
--	begin
--		if clk'event and clk = '1' then
--			if rst_j = '1' then
--				j <= 0;
--			elsif adv_j = '1' then
--				j <= j + 1;
--			end if;
--		end if;
--	end process;
--
-- Counter m ----Transmission bit count ------------------------------------
	process(clk)
	begin
		if clk'event and clk = '1' then
			if rst_m = '1' then
				m <= 0;
			elsif adv_m = '1' then
				m <= m + 1;
			end if;
		end if;
	end process;
--
---- Counter n ----Transmission byte count ------------------------------------
--	process(clk)
--	begin
--		if clk'event and clk = '1' then
--			if rst_n = '1' then
--				n <= 0;
--			elsif adv_n = '1' then
--				n <= n + 1;
--			end if;
--		end if;
--	end process;

-- Counter x ----averaging count ------------------------------------
	process(clk)
	begin
		if clk'event and clk = '1' then
			if rst_x = '1' then
				x <= 0;
			elsif adv_x = '1' then
				x <= x + 1;
			end if;
		end if;
	end process;
	
-- Counter y ---- count ------------------------------------
	process(clk)
	begin
		if clk'event and clk = '1' then
			if rst_y = '1' then
				y <= 0;
			elsif adv_y = '1' then
				y <= y + 1;
			end if;
		end if;
	end process;
	
-- Data Flow Process --------------------------------------------------------	
	
	process(state,st_debounced,i,j,m,n)
	begin
		
		initAll <= '0';
		load_done <= '0';
		load_trigger <= '0';
		rst_i <= '0';
		rst_j <= '0';
		rst_m <= '0';
		rst_n <= '0';
		rst_x <= '0';
		rst_y <= '0';
		adv_i <= '0';
		adv_j <= '0';
		adv_m <= '0';
		adv_n <= '0';
		adv_x <= '0';
		adv_y <= '0';
		load_q <= '0';
		load_plainText <= '0';
		load_plainText_out <= '0';
		load_ledg0 <= '0';
		load_ptBlock_k <= '0';
		load_b_block <= '0';
		load_sBox_block <= '0';
		load_rot_block <= '0';
		load_mc_block <= '0';
		load_xor_block <= '0';
		load_u_cipher <= '0';
		load_cipher <= '0';
		load_TxD <= '0';
		
		case state is
			when 0 =>
				if st_debounced = '0' then
					initAll <= '1';
					rst_i <= '1';
					rst_j <= '1';
					rst_m <= '1';
					rst_n <= '1';
					nextState <= 1;
				else
					load_done <= '1';
					nextState <= 0;
				end if;
			
			when 1 =>
				initAll <= '1';
				nextState <= 2;
				
			when 2 => 
				load_q <= '1';
				nextState <= 3;

			when 3 =>
				load_plainText <= '1';
				nextState <= 4;
				
			when 4 =>
				load_plainText_out <= '1';
				nextState <= 5;
								
-- Begin Serial Port Transmission--------------------------------------------
	
--			when 5 =>	
--				if j = 187500000 then      -- <<<<<<<<Set j loop number of cycles 
--					adv_j <= '1';
--					nextState <= 7;
--				else
--					adv_j <= '1';
--					nextState <= 6;
--				end if;
--			
--			when 6 =>	
--				adv_j <= '1';
--				load_ledg0 <= '1';
--				nextState <= 5;
--								
--			when 7 =>
--				if m = 10 then      -- <<<<<<<<Set m loop number of cycles
--					rst_m <= '1';
--					nextState <= 9;
--				else
--					rst_j <= '1';
--					adv_m <= '1';
--					load_TxD <= '1';
--					nextState <= 8;
--				end if;
--				
--			when 8 =>	
--				adv_m <= '1';
--				load_TxD <= '1';
--				nextState <= 7;
--								
--			when 9 =>
--				if n = 3 then      -- <<<<<<<<<Set n loop number of cycles
--					rst_n <= '1';
--					nextState <= 10;
--				else
--					adv_n <= '1';
--					nextState <= 5;
--				end if;
				
-- End Serial Port Transmission ---------------------------------------------				

			when 5 =>												--this is when trigger is set to high
				load_ptBlock_k <= '1';
				load_trigger <= '1';
				nextState <= 6;
								
			when 6 =>
				load_b_block <= '1';
				nextState <= 7;
								
			when 7 =>												--this is when sbox happens 200-800
				load_sBox_block <= '1';
				nextState <= 8;
								
			when 8 =>
				load_rot_block <= '1';
				nextState <= 9;
								
			when 9 =>
				load_mc_block <= '1';
				nextState <= 10;
								
			when 10 =>
				load_xor_block <= '1';
				nextState <= 11;
				
			when 11 =>
				load_u_cipher <= '1';
				nextState <= 12;
								
			when 12 =>
				load_cipher <= '1';
				if x = 511 then
					rst_x <= '1';
					nextState <= 13;
				else 
					adv_x <= '1';
					nextState <= 16;
				end if;
								
			when 13 =>
				if m = 300000 then      -- <<<<<<<<Set m loop number of cycles / set to ~3 seconds per encryption cycle for a 50MHz clock
					rst_m <= '1';
					nextState <= 2;
				else
					adv_m <= '1';
					load_ledg0 <= '1';
					nextState <= 14;
				end if;
				
			when 14 =>	
				adv_m <= '1';
				load_ledg0 <= '1';
				nextState <= 13;
								
			when 15 =>
				if i = 1000000000 then      -- <<<<<<<<<Set i loop number of cycles
					load_done <= '1';
					nextState <= 0;
				else
					adv_i <= '1';
					nextState <= 2; 
				end if;
			
			when 16 =>
				if y = 1000 then
					rst_y <= '1';
					nextState <= 2;
				else
					adv_y <= '1';
					nextState <= 17;
				end if;
				
			when 17 =>
--				adv_y <= '1';
				nextState <= 16;
				
			when others =>
				nextState <= 0;
		
		end case;	
	end process;
	
	
			
-- Regsiter key ------------------------------------------------------------
key <= x"d4f0cbc3" when clk'event and clk='1' and initAll = '1';

-- Register q --------------------------------------------------------------
q <= seed(7) & seed(6) & seed(5) & seed(4) & seed(3) & seed(2) & seed(1) &
     seed(0) & seed(3) & seed(2) & seed(1) & seed(0) & seed(7) & seed(6) &
	 seed(5) & seed(4) & seed(2) & seed(1) & seed(0) & seed(7) & seed(6) &
	 seed(5) & seed(4) & seed(3) & seed(1) & seed(0) & seed(7) & seed(6) &
	 seed(5) & seed(4) & seed(3) & seed(2)
     when clk'event and clk='1' and initAll = '1' else
	 
	 q(30) & q(29) & q(28) & q(27) & q(26) & q(25) & q(24) & q(23) &
	 q(22) & q(21) & q(20) & q(19) & q(18) & q(17) & q(16) & q(15) &
	 q(14) & q(13) & q(12) & q(11) & q(10) & q(9) & q(8) & q(7) &
	 q(6) & q(5) & q(4) & q(3) & q(2) & q(1) & q(0) &
	 (q(0) xor q(1) xor q(11) xor q(31))
	 when clk'event and clk='1' and load_q = '1' and x = 0 else 
	 
	 plainText
	 when clk'event and clk='1' and load_q = '1' and x /= 0; 

-- Register plaintext ------------------------------------------------------
	plainText <= x"00000000" when clk'event and clk='1' and initAll = '1'
                 else q when clk'event and clk='1' and load_plainText = '1';

-- Register plainText_out --------------------------------------------------				 
	plainText_out <= x"0000000000" when clk'event and clk='1' and 
	                 initAll = '1' else
					 
					 '1' & plainText(31 downto 24) & '0' & '1' &
					 plainText(23 downto 16) & '0' & '1' &
					 plainText(15 downto 8) & '0' & '1' &
					 plainText(7 downto 0) & '0'
					 when clk'event and clk='1' and load_plainText_out='1'
					 else
					 
					 TO_STDLOGICVECTOR(TO_BITVECTOR(plainText_out) srl 1)
					 when clk'event and clk='1' and load_TxD='1';
					 
-- Register trigger -------------------------------------------------------------				 
	trigger <= '0' when clk'event and clk='1' and initAll = '1' else
	
	           '1' when clk'event and clk='1' and load_trigger = '1' else
				  
				  '0' when clk'event and clk='1' and load_trigger = '0';
					 
-- Register TxD -------------------------------------------------------------				 
--	TxD <= '1' when clk'event and clk='1' and initAll = '1' else 
--		
--		   plainText_out(0) when clk'event and clk='1' and load_TxD='1';
				 
-- Registers plaintext block (b) and key block (k) -------------------------
	ptBlock(0,0)(7 downto 0) <= UNSIGNED(plainText(31 downto 24))
	when clk'event and clk='1' and load_ptBlock_k = '1';
	ptBlock(1,0)(7 downto 0) <= UNSIGNED(plainText(23 downto 16))
	when clk'event and clk='1' and load_ptBlock_k = '1';
	ptBlock(0,1)(7 downto 0) <= UNSIGNED(plainText(15 downto 8))
	when clk'event and clk='1' and load_ptBlock_k = '1';
	ptBlock(1,1)(7 downto 0) <= UNSIGNED(plainText(7 downto 0))
	when clk'event and clk='1' and load_ptBlock_k = '1';
			
	k(0,0)(7 downto 0) <= UNSIGNED(key(31 downto 24))
	when clk'event and clk='1' and load_ptBlock_k = '1';
	k(1,0)(7 downto 0) <= UNSIGNED(key(23 downto 16))
	when clk'event and clk='1' and load_ptBlock_k = '1';
	k(0,1)(7 downto 0) <= UNSIGNED(key(15 downto 8))
	when clk'event and clk='1' and load_ptBlock_k = '1';
	k(1,1)(7 downto 0) <= UNSIGNED(key(7 downto 0))
	when clk'event and clk='1' and load_ptBlock_k = '1';
		 	
-- Register b --------------------------------------------------------------

	b <= ((x"00", x"00"), (x"00", x"00"))
	     when clk'event and clk='1' and initAll = '1' else
		
	((ptBlock(0,0) xor k(0,0), ptBlock(0,1) xor k(0,1)),
	(ptBlock(1,0) xor k(1,0), ptBlock(1,1) xor k(1,1))) 
	when clk'event and clk='1' and load_b_block = '1' else
		
	((sBox(TO_INTEGER(b(0,0)(7 downto 4)), TO_INTEGER(b(0,0)(3 downto 0))),
	  sBox(TO_INTEGER(b(0,1)(7 downto 4)), TO_INTEGER(b(0,1)(3 downto 0)))),
	(sBox(TO_INTEGER(b(1,0)(7 downto 4)), TO_INTEGER(b(1,0)(3 downto 0))),
	 sBox(TO_INTEGER(b(1,1)(7 downto 4)), TO_INTEGER(b(1,1)(3 downto 0)))))
	when clk'event and clk='1' and load_sBox_block = '1' else
	
	((b(0,0), b(0,1)), (b(1,1), b(1,0))) 
	when clk'event and clk='1' and load_rot_block = '1' else
	
	(((mc_x_2(TO_INTEGER(b(0,0)(7 downto 4)),
       TO_INTEGER(b(0,0)(3 downto 0)))) xor 
	(mc_x_3(TO_INTEGER(b(1,0)(7 downto 4)),
     TO_INTEGER(b(1,0)(3 downto 0)))), 
	(mc_x_2(TO_INTEGER(b(0,1)(7 downto 4)),
	 TO_INTEGER(b(0,1)(3 downto 0)))) xor 
	(mc_x_3(TO_INTEGER(b(1,1)(7 downto 4)),
	 TO_INTEGER(b(1,1)(3 downto 0))))), 
	((b(0,0)) xor (mc_x_2(TO_INTEGER(b(1,0)(7 downto 4)),
	  TO_INTEGER(b(1,0)(3 downto 0)))), 
	(b(0,1)) xor (mc_x_2(TO_INTEGER(b(1,1)(7 downto 4)),
	 TO_INTEGER(b(1,1)(3 downto 0))))))
	when clk'event and clk='1' and load_mc_block = '1' else
	
	((b(0,0) xor k(0,0), b(0,1) xor k(0,1)),
	 (b(1,0) xor k(1,0), b(1,1) xor k(1,1))) 
	when clk'event and clk='1' and load_xor_block = '1';

-- Register u_cipherText -----------------------------------------------------
	u_cipherText <= x"00000000" when clk'event and clk='1'
	              and initAll = '1' else
	
					  b(0, 0) & b(1, 0) & b(0, 1) & b(1, 1)
					  when clk'event and clk='1' and load_u_cipher = '1';
	
-- Register cipherText -----------------------------------------------------

	cipherText <= x"00000000" when clk'event and clk='1'
	              and initAll = '1' else

	              std_logic_vector(u_cipherText)
	              when clk'event and clk='1' and load_cipher = '1';
				  
-- Register LEDG0 ------------------------------------------------------------

	LEDG0 <= '1' when load_ledg0 = '1' else '0';
	
-- Register done ------------------------------------------------------------

	done <= '1' when load_done = '1' else '0';
	
-- Register hex0 ------------------------------------------------------------	

	hex0 <= b"1000000" when u_cipherText(3 downto 0) = x"0" else
	        b"1111001" when u_cipherText(3 downto 0) = x"1" else
			  b"0100100" when u_cipherText(3 downto 0) = x"2" else
			  b"0110000" when u_cipherText(3 downto 0) = x"3" else
			  b"0011001" when u_cipherText(3 downto 0) = x"4" else
			  b"0010010" when u_cipherText(3 downto 0) = x"5" else
			  b"0000010" when u_cipherText(3 downto 0) = x"6" else
			  b"1111000" when u_cipherText(3 downto 0) = x"7" else
			  b"0000000" when u_cipherText(3 downto 0) = x"8" else
			  b"0010000" when u_cipherText(3 downto 0) = x"9" else
			  b"0001000" when u_cipherText(3 downto 0) = x"a" else
			  b"0000011" when u_cipherText(3 downto 0) = x"b" else
			  b"0100111" when u_cipherText(3 downto 0) = x"c" else
			  b"0100001" when u_cipherText(3 downto 0) = x"d" else
			  b"0000110" when u_cipherText(3 downto 0) = x"e" else
			  b"0001110" when u_cipherText(3 downto 0) = x"f";
			
-- Register hex1 ------------------------------------------------------------	

	hex1 <= b"1000000" when u_cipherText(7 downto 4) = x"0" else
	        b"1111001" when u_cipherText(7 downto 4) = x"1" else
			  b"0100100" when u_cipherText(7 downto 4) = x"2" else
			  b"0110000" when u_cipherText(7 downto 4) = x"3" else
			  b"0011001" when u_cipherText(7 downto 4) = x"4" else
			  b"0010010" when u_cipherText(7 downto 4) = x"5" else
			  b"0000010" when u_cipherText(7 downto 4) = x"6" else
			  b"1111000" when u_cipherText(7 downto 4) = x"7" else
			  b"0000000" when u_cipherText(7 downto 4) = x"8" else
			  b"0010000" when u_cipherText(7 downto 4) = x"9" else
			  b"0001000" when u_cipherText(7 downto 4) = x"a" else
			  b"0000011" when u_cipherText(7 downto 4) = x"b" else
			  b"0100111" when u_cipherText(7 downto 4) = x"c" else
			  b"0100001" when u_cipherText(7 downto 4) = x"d" else
			  b"0000110" when u_cipherText(7 downto 4) = x"e" else
			  b"0001110" when u_cipherText(7 downto 4) = x"f";
			
-- Register hex2 ------------------------------------------------------------	

	hex2 <= b"1000000" when u_cipherText(11 downto 8) = x"0" else
	        b"1111001" when u_cipherText(11 downto 8) = x"1" else
			  b"0100100" when u_cipherText(11 downto 8) = x"2" else
			  b"0110000" when u_cipherText(11 downto 8) = x"3" else
			  b"0011001" when u_cipherText(11 downto 8) = x"4" else
			  b"0010010" when u_cipherText(11 downto 8) = x"5" else
			  b"0000010" when u_cipherText(11 downto 8) = x"6" else
			  b"1111000" when u_cipherText(11 downto 8) = x"7" else
			  b"0000000" when u_cipherText(11 downto 8) = x"8" else
			  b"0010000" when u_cipherText(11 downto 8) = x"9" else
			  b"0001000" when u_cipherText(11 downto 8) = x"a" else
			  b"0000011" when u_cipherText(11 downto 8) = x"b" else
			  b"0100111" when u_cipherText(11 downto 8) = x"c" else
			  b"0100001" when u_cipherText(11 downto 8) = x"d" else
			  b"0000110" when u_cipherText(11 downto 8) = x"e" else
			  b"0001110" when u_cipherText(11 downto 8) = x"f";
			
-- Register hex3 ------------------------------------------------------------	

	hex3 <= b"1000000" when u_cipherText(15 downto 12) = x"0" else
	        b"1111001" when u_cipherText(15 downto 12) = x"1" else
			  b"0100100" when u_cipherText(15 downto 12) = x"2" else
			  b"0110000" when u_cipherText(15 downto 12) = x"3" else
			  b"0011001" when u_cipherText(15 downto 12) = x"4" else
			  b"0010010" when u_cipherText(15 downto 12) = x"5" else
			  b"0000010" when u_cipherText(15 downto 12) = x"6" else
			  b"1111000" when u_cipherText(15 downto 12) = x"7" else
			  b"0000000" when u_cipherText(15 downto 12) = x"8" else
			  b"0010000" when u_cipherText(15 downto 12) = x"9" else
			  b"0001000" when u_cipherText(15 downto 12) = x"a" else
			  b"0000011" when u_cipherText(15 downto 12) = x"b" else
			  b"0100111" when u_cipherText(15 downto 12) = x"c" else
			  b"0100001" when u_cipherText(15 downto 12) = x"d" else
			  b"0000110" when u_cipherText(15 downto 12) = x"e" else
			  b"0001110" when u_cipherText(15 downto 12) = x"f";
			
-- Register hex4 ------------------------------------------------------------	

	hex4 <= b"1000000" when u_cipherText(19 downto 16) = x"0" else
	        b"1111001" when u_cipherText(19 downto 16) = x"1" else
			  b"0100100" when u_cipherText(19 downto 16) = x"2" else
			  b"0110000" when u_cipherText(19 downto 16) = x"3" else
			  b"0011001" when u_cipherText(19 downto 16) = x"4" else
			  b"0010010" when u_cipherText(19 downto 16) = x"5" else
			  b"0000010" when u_cipherText(19 downto 16) = x"6" else
			  b"1111000" when u_cipherText(19 downto 16) = x"7" else
			  b"0000000" when u_cipherText(19 downto 16) = x"8" else
			  b"0010000" when u_cipherText(19 downto 16) = x"9" else
			  b"0001000" when u_cipherText(19 downto 16) = x"a" else
			  b"0000011" when u_cipherText(19 downto 16) = x"b" else
			  b"0100111" when u_cipherText(19 downto 16) = x"c" else
			  b"0100001" when u_cipherText(19 downto 16) = x"d" else
			  b"0000110" when u_cipherText(19 downto 16) = x"e" else
			  b"0001110" when u_cipherText(19 downto 16) = x"f";
			
-- Register hex5 ------------------------------------------------------------	

	hex5 <= b"1000000" when u_cipherText(23 downto 20) = x"0" else
	        b"1111001" when u_cipherText(23 downto 20) = x"1" else
			  b"0100100" when u_cipherText(23 downto 20) = x"2" else
			  b"0110000" when u_cipherText(23 downto 20) = x"3" else
			  b"0011001" when u_cipherText(23 downto 20) = x"4" else
			  b"0010010" when u_cipherText(23 downto 20) = x"5" else
			  b"0000010" when u_cipherText(23 downto 20) = x"6" else
			  b"1111000" when u_cipherText(23 downto 20) = x"7" else
			  b"0000000" when u_cipherText(23 downto 20) = x"8" else
			  b"0010000" when u_cipherText(23 downto 20) = x"9" else
			  b"0001000" when u_cipherText(23 downto 20) = x"a" else
			  b"0000011" when u_cipherText(23 downto 20) = x"b" else
			  b"0100111" when u_cipherText(23 downto 20) = x"c" else
			  b"0100001" when u_cipherText(23 downto 20) = x"d" else
			  b"0000110" when u_cipherText(23 downto 20) = x"e" else
			  b"0001110" when u_cipherText(23 downto 20) = x"f";
			
-- Register hex6 ------------------------------------------------------------	

	hex6 <= b"1000000" when u_cipherText(27 downto 24) = x"0" else
	        b"1111001" when u_cipherText(27 downto 24) = x"1" else
			  b"0100100" when u_cipherText(27 downto 24) = x"2" else
			  b"0110000" when u_cipherText(27 downto 24) = x"3" else
			  b"0011001" when u_cipherText(27 downto 24) = x"4" else
			  b"0010010" when u_cipherText(27 downto 24) = x"5" else
			  b"0000010" when u_cipherText(27 downto 24) = x"6" else
			  b"1111000" when u_cipherText(27 downto 24) = x"7" else
			  b"0000000" when u_cipherText(27 downto 24) = x"8" else
			  b"0010000" when u_cipherText(27 downto 24) = x"9" else
			  b"0001000" when u_cipherText(27 downto 24) = x"a" else
			  b"0000011" when u_cipherText(27 downto 24) = x"b" else
			  b"0100111" when u_cipherText(27 downto 24) = x"c" else
			  b"0100001" when u_cipherText(27 downto 24) = x"d" else
			  b"0000110" when u_cipherText(27 downto 24) = x"e" else
			  b"0001110" when u_cipherText(27 downto 24) = x"f";
			
-- Register hex7 ------------------------------------------------------------	

	hex7 <= b"1000000" when u_cipherText(31 downto 28) = x"0" else
	        b"1111001" when u_cipherText(31 downto 28) = x"1" else
			  b"0100100" when u_cipherText(31 downto 28) = x"2" else
			  b"0110000" when u_cipherText(31 downto 28) = x"3" else
			  b"0011001" when u_cipherText(31 downto 28) = x"4" else
			  b"0010010" when u_cipherText(31 downto 28) = x"5" else
			  b"0000010" when u_cipherText(31 downto 28) = x"6" else
			  b"1111000" when u_cipherText(31 downto 28) = x"7" else
			  b"0000000" when u_cipherText(31 downto 28) = x"8" else
			  b"0010000" when u_cipherText(31 downto 28) = x"9" else
			  b"0001000" when u_cipherText(31 downto 28) = x"a" else
			  b"0000011" when u_cipherText(31 downto 28) = x"b" else
			  b"0100111" when u_cipherText(31 downto 28) = x"c" else
			  b"0100001" when u_cipherText(31 downto 28) = x"d" else
			  b"0000110" when u_cipherText(31 downto 28) = x"e" else
			  b"0001110" when u_cipherText(31 downto 28) = x"f";

end dataflow_encrypt;