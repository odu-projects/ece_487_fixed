# Inherited 32 Bit design

I don't know if versioning this quartus project will be successful, but I'm going to give it a shot. This is the "proof of concept" trimmed down 32 bit AES we have been kicking around for some time now. As a last ditch effort, I am going to try and cram a NIOS 2 in here and see if can hold stuff on the board in reset in the software because doing it in the hardware looks to be a trip.

Anyhow, we'll see. Whatever I leave I will make sure it doesn't interfere with anyone's ability to push or pull.

--Micah Lehman 11/20/2019
