# Aldec Folder	

Here are the parts necessary for the large AES 128 and the compact AES 128 developed by Carlo. More details in the folders.

The 32-bit "AES" we inherited is included as a quartus project in the correct folder, and you will find it on the lab computer in the appropriate folder on the desktop. We decided not to include it in the repo.
