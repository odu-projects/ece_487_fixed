library IEEE;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

package LUT_package is

	type LUT_256b1 is array (0 to 255) of std_logic_vector(7 downto 0);
	type LUT_11b1 is array  (0 to 10) of std_logic_vector(7 downto 0);
	type LUT_key is array  (0 to 10) of std_logic_vector(127 downto 0);
	subtype bit_128 is std_logic_vector(127 downto 0);
	subtype word is std_logic_vector(31 downto 0);
	subtype byte is std_logic_vector(7 downto 0);


	function subBytes (a : bit_128) return bit_128;
	function shiftRows (a : bit_128) return bit_128;
	function mixColumns (a : bit_128) return bit_128;
	function addRoundKey(a, b: bit_128) return bit_128;
	function RotWord (a : word) return word;
	function subBytes_32 (a : word) return word;
	function expand_key(a: bit_128; b: word) return bit_128;

end package LUT_package;

package body LUT_package is

	constant sbox_LUT: LUT_256b1:= (
		X"63", X"7C", X"77", X"7B", X"F2", X"6B", X"6F", X"C5", X"30", X"01", X"67", X"2B", X"FE", X"D7", X"AB", X"76",
		X"CA", X"82", X"C9", X"7D", X"FA", X"59", X"47", X"F0", X"AD", X"D4", X"A2", X"AF", X"9C", X"A4", X"72", X"C0",
		X"B7", X"FD", X"93", X"26", X"36", X"3F", X"F7", X"CC", X"34", X"A5", X"E5", X"F1", X"71", X"D8", X"31", X"15",
		X"04", X"C7", X"23", X"C3", X"18", X"96", X"05", X"9A", X"07", X"12", X"80", X"E2", X"EB", X"27", X"B2", X"75",
		X"09", X"83", X"2C", X"1A", X"1B", X"6E", X"5A", X"A0", X"52", X"3B", X"D6", X"B3", X"29", X"E3", X"2F", X"84",
		X"53", X"D1", X"00", X"ED", X"20", X"FC", X"B1", X"5B", X"6A", X"CB", X"BE", X"39", X"4A", X"4C", X"58", X"CF",
		X"D0", X"EF", X"AA", X"FB", X"43", X"4D", X"33", X"85", X"45", X"F9", X"02", X"7F", X"50", X"3C", X"9F", X"A8",
		X"51", X"A3", X"40", X"8F", X"92", X"9D", X"38", X"F5", X"BC", X"B6", X"DA", X"21", X"10", X"FF", X"F3", X"D2",
		X"CD", X"0C", X"13", X"EC", X"5F", X"97", X"44", X"17", X"C4", X"A7", X"7E", X"3D", X"64", X"5D", X"19", X"73",
		X"60", X"81", X"4F", X"DC", X"22", X"2A", X"90", X"88", X"46", X"EE", X"B8", X"14", X"DE", X"5E", X"0B", X"DB",
		X"E0", X"32", X"3A", X"0A", X"49", X"06", X"24", X"5C", X"C2", X"D3", X"AC", X"62", X"91", X"95", X"E4", X"79",
		X"E7", X"C8", X"37", X"6D", X"8D", X"D5", X"4E", X"A9", X"6C", X"56", X"F4", X"EA", X"65", X"7A", X"AE", X"08",
		X"BA", X"78", X"25", X"2E", X"1C", X"A6", X"B4", X"C6", X"E8", X"DD", X"74", X"1F", X"4B", X"BD", X"8B", X"8A",
		X"70", X"3E", X"B5", X"66", X"48", X"03", X"F6", X"0E", X"61", X"35", X"57", X"B9", X"86", X"C1", X"1D", X"9E",
		X"E1", X"F8", X"98", X"11", X"69", X"D9", X"8E", X"94", X"9B", X"1E", X"87", X"E9", X"CE", X"55", X"28", X"DF",
		X"8C", X"A1", X"89", X"0D", X"BF", X"E6", X"42", X"68", X"41", X"99", X"2D", X"0F", X"B0", X"54", X"BB", X"16"
	);

	constant mult_3 : LUT_256b1 := (
		x"00",x"03",x"06",x"05",x"0C",x"0F",x"0A",x"09",x"18",x"1B",x"1E",x"1D",x"14",x"17",x"12",x"11",
		x"30",x"33",x"36",x"35",x"3C",x"3F",x"3A",x"39",x"28",x"2B",x"2E",x"2D",x"24",x"27",x"22",x"21",
		x"60",x"63",x"66",x"65",x"6C",x"6F",x"6A",x"69",x"78",x"7B",X"7E",x"7D",x"74",x"77",x"72",x"71",
		x"50",x"53",x"56",x"55",x"5C",x"5F",x"5A",x"59",x"48",x"4B",x"4E",x"4D",x"44",x"47",x"42",x"41",
		x"C0",x"C3",x"C6",x"C5",x"CC",x"CF",x"CA",x"C9",x"D8",x"DB",x"DE",x"DD",x"D4",x"D7",x"D2",x"D1",
		x"F0",x"F3",x"F6",x"F5",x"FC",x"FF",x"FA",x"F9",x"E8",x"EB",x"EE",x"ED",x"E4",x"E7",x"E2",x"E1",
		x"A0",x"A3",x"A6",x"A5",x"AC",x"AF",x"AA",x"A9",x"B8",x"BB",x"BE",x"BD",x"B4",x"B7",x"B2",x"B1",
		x"90",x"93",x"96",x"95",x"9C",x"9F",x"9A",x"99",x"88",x"8B",x"8E",x"8D",x"84",x"87",x"82",x"81",
		x"9B",x"98",x"9D",x"9E",x"97",x"94",x"91",x"92",x"83",x"80",x"85",x"86",x"8F",x"8C",x"89",x"8A",
		x"AB",x"A8",x"AD",x"AE",x"A7",x"A4",x"A1",x"A2",x"B3",x"B0",x"B5",x"B6",x"BF",x"BC",x"B9",x"BA",
		x"FB",x"F8",x"FD",x"FE",x"F7",x"F4",x"F1",x"F2",x"E3",x"E0",x"E5",x"E6",x"EF",x"EC",x"E9",x"EA",
		x"CB",x"C8",x"CD",x"CE",x"C7",x"C4",x"C1",x"C2",x"D3",x"D0",x"D5",x"D6",x"DF",x"DC",x"D9",x"DA",
		x"5B",x"58",x"5D",x"5E",x"57",x"54",x"51",x"52",x"43",x"40",x"45",x"46",x"4F",x"4C",x"49",x"4A",
		x"6B",x"68",x"6D",x"6E",x"67",x"64",x"61",x"62",x"73",x"70",x"75",x"76",x"7F",x"7C",x"79",x"7A",
		x"3B",x"38",x"3D",x"3E",x"37",x"34",x"31",x"32",x"23",x"20",x"25",x"26",x"2F",x"2C",x"29",x"2A",
		x"0B",x"08",x"0D",x"0E",x"07",x"04",x"01",x"02",x"13",x"10",x"15",x"16",x"1F",x"1C",x"19",x"1A"
	);

	constant mult_2 : LUT_256b1 := (
		X"00",X"02",X"04",X"06",X"08",X"0A",X"0C",X"0E",X"10",X"12",X"14",X"16",X"18",X"1A",X"1C",X"1E",
		X"20",X"22",X"24",X"26",X"28",X"2A",X"2C",X"2E",X"30",X"32",X"34",X"36",X"38",X"3A",X"3C",X"3E",
		X"40",x"42",X"44",X"46",X"48",X"4A",X"4C",X"4E",X"50",X"52",X"54",X"56",X"58",X"5A",X"5C",X"5E",
		X"60",X"62",X"64",X"66",X"68",X"6A",X"6C",X"6E",X"70",X"72",X"74",X"76",X"78",X"7A",X"7C",X"7E",
		X"80",X"82",X"84",X"86",X"88",X"8A",X"8C",X"8E",X"90",X"92",X"94",X"96",X"98",X"9A",X"9C",X"9E",
		X"A0",X"A2",X"A4",X"A6",X"A8",X"AA",X"AC",X"AE",X"B0",X"B2",X"B4",X"B6",X"B8",X"BA",X"BC",X"BE",
		X"C0",X"C2",X"C4",X"C6",X"C8",X"CA",X"CC",X"CE",X"D0",X"D2",X"D4",X"D6",X"D8",X"DA",X"DC",X"DE",
		X"E0",X"E2",X"E4",X"E6",X"E8",X"EA",X"EC",X"EE",X"F0",X"F2",X"F4",X"F6",X"F8",X"FA",X"FC",X"FE",
		X"1B",X"19",X"1F",X"1D",X"13",X"11",X"17",X"15",X"0B",X"09",X"0F",X"0D",X"03",X"01",X"07",X"05",
		X"3B",X"39",X"3F",X"3D",X"33",X"31",X"37",X"35",X"2B",X"29",X"2F",X"2D",X"23",X"21",X"27",X"25",
		X"5B",X"59",X"5F",X"5D",X"53",X"51",X"57",X"55",X"4B",X"49",X"4F",X"4D",X"43",X"41",X"47",X"45",
		X"7B",X"79",X"7F",X"7D",X"73",X"71",X"77",X"75",X"6B",X"69",X"6F",X"6D",X"63",X"61",X"67",X"65",
		X"9B",X"99",X"9F",X"9D",X"93",X"91",X"97",X"95",X"8B",X"89",X"8F",X"8D",X"83",X"81",X"87",X"85",
		X"BB",X"B9",X"BF",X"BD",X"B3",X"B1",X"B7",X"B5",X"AB",X"A9",X"AF",X"AD",X"A3",X"A1",X"A7",X"A5",
		X"DB",X"D9",X"DF",X"DD",X"D3",X"D1",X"D7",X"D5",X"CB",X"C9",X"CF",X"CD",X"C3",X"C1",X"C7",X"C5",
		X"FB",X"F9",X"FF",X"FD",X"F3",X"F1",X"F7",X"F5",X"EB",X"E9",X"EF",X"ED",X"E3",X"E1",X"E7",X"E5"
	);

	constant mul_13: LUT_256b1:= (
		X"00", X"0d", X"1a", X"17", X"34", X"39", X"2e", X"23", X"68", X"65", X"72", X"7f", X"5c", X"51", X"46", X"4b",
		X"d0", X"dd", X"ca", X"c7", X"e4", X"e9", X"fe", X"f3", X"b8", X"b5", X"a2", X"af", X"8c", X"81", X"96", X"9b",
		X"bb", X"b6", X"a1", X"ac", X"8f", X"82", X"95", X"98", X"d3", X"de", X"c9", X"c4", X"e7", X"ea", X"fd", X"f0",
		X"6b", X"66", X"71", X"7c", X"5f", X"52", X"45", X"48", X"03", X"0e", X"19", X"14", X"37", X"3a", X"2d", X"20",
		X"6d", X"60", X"77", X"7a", X"59", X"54", X"43", X"4e", X"05", X"08", X"1f", X"12", X"31", X"3c", X"2b", X"26",
		X"bd", X"b0", X"a7", X"aa", X"89", X"84", X"93", X"9e", X"d5", X"d8", X"cf", X"c2", X"e1", X"ec", X"fb", X"f6",
		X"d6", X"db", X"cc", X"c1", X"e2", X"ef", X"f8", X"f5", X"be", X"b3", X"a4", X"a9", X"8a", X"87", X"90", X"9d",
		X"06", X"0b", X"1c", X"11", X"32", X"3f", X"28", X"25", X"6e", X"63", X"74", X"79", X"5a", X"57", X"40", X"4d",
		X"da", X"d7", X"c0", X"cd", X"ee", X"e3", X"f4", X"f9", X"b2", X"bf", X"a8", X"a5", X"86", X"8b", X"9c", X"91",
		X"0a", X"07", X"10", X"1d", X"3e", X"33", X"24", X"29", X"62", X"6f", X"78", X"75", X"56", X"5b", X"4c", X"41",
		X"61", X"6c", X"7b", X"76", X"55", X"58", X"4f", X"42", X"09", X"04", X"13", X"1e", X"3d", X"30", X"27", X"2a",
		X"b1", X"bc", X"ab", X"a6", X"85", X"88", X"9f", X"92", X"d9", X"d4", X"c3", X"ce", X"ed", X"e0", X"f7", X"fa",
		X"b7", X"ba", X"ad", X"a0", X"83", X"8e", X"99", X"94", X"df", X"d2", X"c5", X"c8", X"eb", X"e6", X"f1", X"fc",
		X"67", X"6a", X"7d", X"70", X"53", X"5e", X"49", X"44", X"0f", X"02", X"15", X"18", X"3b", X"36", X"21", X"2c",
		X"0c", X"01", X"16", X"1b", X"38", X"35", X"22", X"2f", X"64", X"69", X"7e", X"73", X"50", X"5d", X"4a", X"47",
		X"dc", X"d1", X"c6", X"cb", X"e8", X"e5", X"f2", X"ff", X"b4", X"b9", X"ae", X"a3", X"80", X"8d", X"9a", X"97"
	);

	constant mul_14: LUT_256b1:= (
		X"00", X"0e", X"1c", X"12", X"38", X"36", X"24", X"2a", X"70", X"7e", X"6c", X"62", X"48", X"46", X"54", X"5a",
		X"e0", X"ee", X"fc", X"f2", X"d8", X"d6", X"c4", X"ca", X"90", X"9e", X"8c", X"82", X"a8", X"a6", X"b4", X"ba",
		X"db", X"d5", X"c7", X"c9", X"e3", X"ed", X"ff", X"f1", X"ab", X"a5", X"b7", X"b9", X"93", X"9d", X"8f", X"81",
		X"3b", X"35", X"27", X"29", X"03", X"0d", X"1f", X"11", X"4b", X"45", X"57", X"59", X"73", X"7d", X"6f", X"61",
		X"ad", X"a3", X"b1", X"bf", X"95", X"9b", X"89", X"87", X"dd", X"d3", X"c1", X"cf", X"e5", X"eb", X"f9", X"f7",
		X"4d", X"43", X"51", X"5f", X"75", X"7b", X"69", X"67", X"3d", X"33", X"21", X"2f", X"05", X"0b", X"19", X"17",
		X"76", X"78", X"6a", X"64", X"4e", X"40", X"52", X"5c", X"06", X"08", X"1a", X"14", X"3e", X"30", X"22", X"2c",
		X"96", X"98", X"8a", X"84", X"ae", X"a0", X"b2", X"bc", X"e6", X"e8", X"fa", X"f4", X"de", X"d0", X"c2", X"cc",
		X"41", X"4f", X"5d", X"53", X"79", X"77", X"65", X"6b", X"31", X"3f", X"2d", X"23", X"09", X"07", X"15", X"1b",
		X"a1", X"af", X"bd", X"b3", X"99", X"97", X"85", X"8b", X"d1", X"df", X"cd", X"c3", X"e9", X"e7", X"f5", X"fb",
		X"9a", X"94", X"86", X"88", X"a2", X"ac", X"be", X"b0", X"ea", X"e4", X"f6", X"f8", X"d2", X"dc", X"ce", X"c0",
		X"7a", X"74", X"66", X"68", X"42", X"4c", X"5e", X"50", X"0a", X"04", X"16", X"18", X"32", X"3c", X"2e", X"20",
		X"ec", X"e2", X"f0", X"fe", X"d4", X"da", X"c8", X"c6", X"9c", X"92", X"80", X"8e", X"a4", X"aa", X"b8", X"b6",
		X"0c", X"02", X"10", X"1e", X"34", X"3a", X"28", X"26", X"7c", X"72", X"60", X"6e", X"44", X"4a", X"58", X"56",
		X"37", X"39", X"2b", X"25", X"0f", X"01", X"13", X"1d", X"47", X"49", X"5b", X"55", X"7f", X"71", X"63", X"6d",
		X"d7", X"d9", X"cb", X"c5", X"ef", X"e1", X"f3", X"fd", X"a7", X"a9", X"bb", X"b5", X"9f", X"91", X"83", X"8d"
	);

	constant rCon: LUT_11b1:=(
		X"8d",  X"01",  X"02",  X"04",  X"08",  X"10",  X"20",  X"40",  X"80",  X"1b",  X"36"
	);
	



	function subBytes (a : bit_128) return bit_128 is
		variable temp : bit_128;
	begin
		for i in 15 downto 0 loop	--perform s-box substitution on all 16 bytes of the input
			temp(i * 8 + 7 downto i * 8) := sbox_LUT(to_integer(unsigned(a(i * 8 + 7 downto i * 8))));
		end loop;

		return temp;  --return input as 128-bit vector
	end subBytes;

	function shiftRows (a : bit_128) return bit_128 is
		variable temp : bit_128;
		alias s00 is a(127 downto 120);
		alias s10 is a(119 downto 112);
		alias s20 is a(111 downto 104);
		alias s30 is a(103 downto 96);
		alias s01 is a(95 downto 88);
		alias s11 is a(87 downto 80);
		alias s21 is a(79 downto 72);
		alias s31 is a(71 downto 64);
		alias s02 is a(63 downto 56);
		alias s12 is a(55 downto 48);
		alias s22 is a(47 downto 40);
		alias s32 is a(39 downto 32);
		alias s03 is a(31 downto 24);
		alias s13 is a(23 downto 16);
		alias s23 is a(15 downto 8);
		alias s33 is a(7 downto 0);

	begin
		temp := s00 & s11 & s22 & s33 & s01 & s12 & s23 & s30 &
				s02 & s13 & s20 & s31 & s03 & s10 & s21 & s32;

		return temp;
	end shiftRows;

	function mixColumns (a : bit_128) return bit_128 is
		variable temp : bit_128;
		alias s00 is a(127 downto 120);
		alias s10 is a(119 downto 112);
		alias s20 is a(111 downto 104);
		alias s30 is a(103 downto 96);
		alias s01 is a(95 downto 88);
		alias s11 is a(87 downto 80);
		alias s21 is a(79 downto 72);
		alias s31 is a(71 downto 64);
		alias s02 is a(63 downto 56);
		alias s12 is a(55 downto 48);
		alias s22 is a(47 downto 40);
		alias s32 is a(39 downto 32);
		alias s03 is a(31 downto 24);
		alias s13 is a(23 downto 16);
		alias s23 is a(15 downto 8);
		alias s33 is a(7 downto 0);
	begin
		-- multiplication matrix for mix columns
		-- 2	3	1	1
		-- 1	2	3	1
		-- 1	1	2	3
		-- 3	1	1	3
		------------------------

		-- first column
		temp(127 downto 120) := mult_2(to_integer(unsigned(s00))) xor mult_3(to_integer(unsigned(s10))) xor S20 xor S30;
		temp(119 downto 112) := S00 xor mult_2(to_integer(unsigned(s10))) xor mult_3(to_integer(unsigned(s20))) xor S30;
		temp(111 downto 104) := S00 xor S10 xor mult_2(to_integer(unsigned(s20))) xor mult_3(to_integer(unsigned(s30)));
		temp(103 downto 96) := mult_3(to_integer(unsigned(s00))) xor S10 xor S20 xor mult_2(to_integer(unsigned(s30)));
		-- second column
		temp(95 downto 88) := mult_2(to_integer(unsigned(s01))) xor mult_3(to_integer(unsigned(s11))) xor S21 xor S31;
		temp(87 downto 80) := S01 xor mult_2(to_integer(unsigned(s11))) xor mult_3(to_integer(unsigned(s21))) xor S31;
		temp(79 downto 72) := S01 xor S11 xor mult_2(to_integer(unsigned(s21))) xor mult_3(to_integer(unsigned(s31)));
		temp(71 downto 64) := mult_3(to_integer(unsigned(s01))) xor S11 xor S21 xor mult_2(to_integer(unsigned(s31)));
		-- third column
		temp(63 downto 56) := mult_2(to_integer(unsigned(s02))) xor mult_3(to_integer(unsigned(s12))) xor S22 xor S32;
		temp(55 downto 48) := S02 xor mult_2(to_integer(unsigned(s12))) xor mult_3(to_integer(unsigned(s22))) xor S32;
		temp(47 downto 40) := S02 xor S12 xor mult_2(to_integer(unsigned(s22))) xor mult_3(to_integer(unsigned(s32)));
		temp(39 downto 32) := mult_3(to_integer(unsigned(s02))) xor S12 xor S22 xor mult_2(to_integer(unsigned(s32)));
		-- fourth column
		temp(31 downto 24) := mult_2(to_integer(unsigned(s03))) xor mult_3(to_integer(unsigned(s13))) xor S23 xor S33;
		temp(23 downto 16) := S03 xor mult_2(to_integer(unsigned(s13))) xor mult_3(to_integer(unsigned(s23))) xor S33;
		temp(15 downto 8) := S03 xor S13 xor mult_2(to_integer(unsigned(s23))) xor mult_3(to_integer(unsigned(s33)));
		temp(7 downto 0) := mult_3(to_integer(unsigned(s03))) xor S13 xor S23 xor mult_2(to_integer(unsigned(s33)));

		return temp;
	end mixColumns;	
	
	
	function addRoundKey(a, b: bit_128) return bit_128 is
		variable temp: bit_128;	 
	
	begin
		temp := a xor b;
		return temp;
	end addRoundKey;
	
	
	function RotWord (a : word) return word is
		variable temp: word;
		
	begin
		temp:= a(23 downto 0) & a(31 downto 24);
		return temp;
	end RotWord;
	
	function subBytes_32 (a : word) return word is
		variable temp : word;
	begin
		for i in 3 downto 0 loop	--perform s-box substitution on all 4 bytes of the input
			temp(i * 8 + 7 downto i * 8) := sbox_LUT(to_integer(unsigned(a(i * 8 + 7 downto i * 8))));
		end loop;

		return temp;  --return input as 128-bit vector
	end subBytes_32; 
	
	function expand_key(a: bit_128; b: word) return bit_128 is
		variable temp: bit_128;
	begin
		temp(127 downto 96) := (a(127 downto 96) xor b);
		temp(95 downto 64) := (a(95 downto 64) xor temp(127 downto 96)); 
		temp(63 downto 32) := (a(63 downto 32) xor temp(95 downto 64));
		temp(31 downto 0) := (a(31 downto 0) xor temp(63 downto 32));
		return temp;
		
	end expand_key;


end package body LUT_package;
















