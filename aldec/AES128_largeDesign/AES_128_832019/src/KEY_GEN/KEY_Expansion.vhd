library IEEE;
library aes_128_832019;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.LUT_package.all; 
use std.textio.all;

entity key_exp is
	port 	(clk: in std_logic 
			);
				
end entity key_exp;

architecture behave of key_exp is		  

	signal key: bit_128;
	signal state, nextState: integer range 0 to 7;	
	signal round: integer:= 0;
	signal load_key, rotateWord, subByte, load_rcon, load_temp, done: std_logic;
	signal rotWordResult, subByteResult,rconResult, temp:	word;

	type expandedKeyArray is array (0 to 10) of bit_128; 
	signal expandedKeys: expandedKeyArray;
	constant rCon: LUT_11b1:=(
		X"8d",  X"01",  X"02",  X"04",  X"08",  X"10",  X"20",  X"40",  X"80",  X"1b",  X"36"
	); 
	
	-- change file location as necessary
	file outfile: text open write_mode is "src\KEY_GEN\expanded_key.txt";
	
begin
	-- change key value as needed. using key value provided in AES manual as text key 
--	key <= x"2B7E151628AED2A6ABF7158809CF4F3C";	--for testing proper implementation
	
	key <= x"00000000000000000000000000000000";
	
	process(clk)
	begin
		if (clk = '1' and clk'event) then
			state <= nextState;		
		end if;
	end process;

	process(state) 
		variable current_line : line;
	begin

		load_key	<= '0';
		done		<= '0';
		rotateWord 	<= '0';
		subByte 	<= '0';
		load_rcon	<= '0';
		load_temp 	<= '0';

		case state is
			when 0 =>
				round <= 0;
				load_key <= '1';
				nextState <= 1;
			when 1 => 
				round <= round + 1;
				nextState <= 2;	
			when 2 =>
				rotateWord <= '1';
				nextState <= 3;	
			when 3 =>
				subByte <= '1';
				nextState <= 4;	
			when 4 =>
				load_rcon <= '1';
				nextState <= 5;	
			when 5 =>
				load_temp <= '1';
				nextState <= 6;
			when 6 =>
				if(round = 10) then 
					done <= '1';  --done
					nextState <= 7;
				else
					nextState <= 1;
				end if;
			when 7 => 	 				-- write expanded keys to a text file and stay in state 7 forever
				for k in 0 to 10 loop
					write(current_line, "0x" & to_hstring(expandedKeys(k)));
					writeline(outfile, current_line);
				end loop;
				nextState <= 7;
		end case;
	end process; 

	---------------------begin combinational logic----------------
	rotWordResult <= rotWord(expandedKeys(round-1)(31 downto 0)) when clk'event and clk = '1' and rotateWord = '1';
	subByteResult <= subBytes_32(rotWordResult) when clk'event and clk = '1' and subByte = '1';
	rconResult <= (subByteResult(31 downto 24) xor rCon(round)) & subByteResult(23 downto 0) when clk'event and clk = '1' and load_rcon = '1';
	expandedKeys(round) <= expand_key(expandedkeys(round-1), rconResult)
						when clk'event and clk = '1' and load_temp = '1' else key when clk'event and clk = '1' and load_key = '1';
		
end behave;