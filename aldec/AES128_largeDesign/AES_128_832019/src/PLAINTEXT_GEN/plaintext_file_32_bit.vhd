library IEEE;
library aes_128_832019;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.LUT_package.all; 
use std.textio.all;

entity plaintext_generator_32_bit is
	port 	(clk: in std_logic 
			);
				
end entity plaintext_generator_32_bit;

architecture behave of plaintext_generator_32_bit is		  

	signal count: integer:= 0;	
	constant numPlain: integer:= 10000;
	file outfile: text open write_mode is "src\PLAINTEXT_GEN\plaintext_32.txt";
	signal plaintext :word := X"6AA64D9A"; 	  --initial value
	
 begin

	 
	process(clk) 
		variable current_line : line; 
		variable bit0 : std_logic; 
	begin
		if (clk'event and clk = '1') then
			if(count < numPlain) then
			  bit0:= plaintext(31) xor plaintext(11) xor plaintext(1) xor plaintext(0);
			  plaintext <= plaintext(30 downto 0) & bit0;
			  if(count > 0) then
				  write(current_line, to_hstring(plaintext(31 downto 24)) & " " & to_hstring(plaintext(23 downto 16)) & " " &
						to_hstring(plaintext(15 downto 8)) & " " & to_hstring(plaintext(7 downto 0))
						);
				  writeline(outfile, current_line);	
			  end if;
			  count <= count +1;
			end if;	
		end if;
	end process;
				
end behave;