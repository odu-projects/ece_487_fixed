library IEEE;
library aes_128_832019;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.LUT_package.all; 
use std.textio.all;

entity plaintext_generator is
	port 	(clk: in std_logic 
			);
				
end entity plaintext_generator;

architecture behave of plaintext_generator is		  

	signal count: integer:= 0;	
	constant numPlain: integer:= 10000;
	file outfile: text open write_mode is "src\PPLAINTEXT_GEN\plaintext.txt";
	signal plaintext :bit_128 := X"12341234ABCDABCDCDEFCDEF12345678"; 	  --initial value
	
 begin

	 
	process(clk) 
		variable current_line : line; 
		variable bit0 : std_logic; 
	begin
		if (clk'event and clk = '1') then
			if(count < numPlain) then
			  bit0:= plaintext(127) xor plaintext(126) xor plaintext(125) xor plaintext(120);
			  plaintext <= plaintext(126 downto 0) & bit0;
			  if(count > 0) then
				  write(current_line, to_hstring(plaintext(127 downto 120)) & " " & to_hstring(plaintext(119 downto 112)) & " " &
				  		to_hstring(plaintext(111 downto 104)) & " " & to_hstring(plaintext(103 downto 96)) & " " &
				  		to_hstring(plaintext(95 downto 88)) & " " & to_hstring(plaintext(87 downto 80)) & " " &	
						to_hstring(plaintext(79 downto 72)) & " " & to_hstring(plaintext(71 downto 64)) & " " &
				  		to_hstring(plaintext(63 downto 56)) & " " & to_hstring(plaintext(55 downto 48)) & " " &
						to_hstring(plaintext(47 downto 40)) & " " & to_hstring(plaintext(39 downto 32)) & " " &
				  		to_hstring(plaintext(31 downto 24)) & " " & to_hstring(plaintext(23 downto 16)) & " " &
						to_hstring(plaintext(15 downto 8)) & " " & to_hstring(plaintext(7 downto 0))
						);
				  writeline(outfile, current_line);	
			  end if;
			  count <= count +1;
			end if;	
		end if;
	end process;
				
end behave;