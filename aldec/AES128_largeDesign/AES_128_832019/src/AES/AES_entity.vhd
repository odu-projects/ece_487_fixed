 library IEEE;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.LUT_package.all;

entity aes_top is
	GENERIC	(	key_size:				in  integer:= 128);
	port 	(--	key:					in 	std_logic_vector( key_size - 1 downto 0);	 --key reg will take maximum of 256, depending on keysize signal, logic will pare down key for operations
				plainText:				in	std_logic_vector( 127 downto 0);			 --will come from LFSR
				clk:			 		in 	std_logic;									 -- start and rst will come from NIOS II softcore
				control_reg:			in  std_logic_vector( 7 downto 0 );
				cipherText:				out std_logic_vector( 127 downto 0 );
				status_reg: 			out std_logic_vector( 7 downto 0 );
				pt_request:				out std_logic;
				pt_status:				in  std_logic;
				busy:					out std_logic
			);

end entity aes_top;

architecture control of aes_top is

type state is (request_pt, request_key, init0, initAddRound, subBytes1, shiftRows2, mixCol3, addRoundKey4, complete5, pt_wait, key_wait, rcv_key);

signal phase, nextPhase:			 		state;
signal plainText_input, cipherText_output: 	std_logic_vector(127 downto 0);
signal state_array:							std_logic_vector(127 downto 0);
signal load_plaintext, load_key, init:  	std_logic;
signal load_subBytes, load_shiftRows:		std_logic;
signal load_mixCol, load_addRoundKey:		std_logic;
signal load_done:							std_logic;
signal load_ptRequest, load_keyRequest:		std_logic;
signal round_count:							integer;
signal counter:								integer:= 0;
signal cycle:								std_logic;
signal round0addRound:						std_logic;
signal key_request:								std_logic;

signal subByteResult, shiftRowResult:		std_logic_vector(127 downto 0);
signal mixColumnResult, AddRoundKeyResult:	std_logic_vector(127 downto 0);	 

--constant roundKey: LUT_key:=(
--	X"000102030405060708090A0B0C0D0E0F",
--	X"D6AA74FDD2AF72FADAA678F1D6AB76FE",
--	X"B692CF0B643DBDF1BE9BC5006830B3FE",
--	X"B6FF744ED2C2C9BF6C590CBF0469BF41",
--	X"47F7F7BC95353E03F96C32BCFD058DFD",
--	X"3CAAA3E8A99F9DEB50F3AF57ADF622AA",
--	X"5E390F7DF7A69296A7553DC10AA31F6B",
--	X"14F9701AE35FE28C440ADF4D4EA9C026",
--	X"47438735A41C65B9E016BAF4AEBF7AD2",
--	X"549932D1F08557681093ED9CBE2C974E",
--	X"13111D7FE3944A17F307A78B4D2B30C5"
--);

constant roundKey: LUT_key:=(
	X"00000000000000000000000000000000",
	X"62636363626363636263636362636363",
	X"9B9898C9F9FBFBAA9B9898C9F9FBFBAA",
	X"90973450696CCFFAF2F457330B0FAC99",
	X"EE06DA7B876A1581759E42B27E91EE2B",
	X"7F2E2B88F8443E098DDA7CBBF34B9290",
	X"EC614B851425758C99FF09376AB49BA7",
	X"217517873550620BACAF6B3CC61BF09B",
	X"0EF903333BA9613897060A04511DFA9F",
	X"B1D4D8E28A7DB9DA1D7BB3DE4C664941",
	X"B4EF5BCB3E92E21123E951CF6F8F188E"
);

begin

	round_count <= 	10 when key_size = 128 else
					12 when key_size = 192 else
					14 when key_size = 256;

	process(clk)			--state control
	begin
		if(clk = '1' and clk'event) then
			phase <= nextPhase;
		end if;
	end process;

	process(phase, control_reg) 			--encryption control
	begin

		load_plaintext 		<= '0';
		load_key			<= '0';
		init				<= '0';
		load_subBytes 		<= '0';
		load_shiftRows		<= '0';
		load_mixCol			<= '0';
		load_addRoundKey	<= '0';
		load_done			<= '0';
		load_keyRequest		<= '0';
		load_ptRequest		<= '0';
		round0addRound		<= '0';
		pt_request			<= '0';		  --goes to LFSR
		key_request			<= '0';		  --goes to NIOS II

		case phase is
			when request_pt =>
				load_ptRequest <= '1';
				nextPhase <= pt_wait;
			when pt_wait =>
				pt_request <= '1';
				if(pt_status = '1') then
					nextPhase <= init0;
				else
					nextPhase <= pt_wait;
				end if;
			when init0 =>
				load_plaintext <= '1';
				nextPhase <= request_key;
			when request_key =>
				load_keyRequest <= '1';
				nextPhase <= key_wait;
			when key_wait =>
				key_request <= '1';
				if(control_reg = X"02") then
					nextPhase <= rcv_key;
				else
					nextPhase <= key_wait;
				end if;
			when rcv_key =>
				load_key <= '1';
				if(counter = 0) then
					nextPhase <= initAddRound;
				else
					nextPhase <= subBytes1;
				end if;
			when initAddRound =>
				round0addRound <= '1';
		--		counter <= counter + 1;
				nextPhase <= subBytes1;
			when subBytes1 =>
				counter <= counter + 1;
				load_subBytes <= '1';
				nextPhase <= shiftRows2;
			when shiftRows2 =>
				load_shiftRows <= '1';
				if(counter = round_count) then
					nextPhase <= addRoundkey4;
				else
					nextPhase <= mixCol3;
				end if;
			when mixCol3 =>					--might break up into more than one cycle
				load_mixCol <= '1';
				nextPhase <= addRoundKey4;
			when addRoundKey4 =>
				load_addRoundKey <= '1';
				if(counter < round_count) then
		--			counter <= counter + 1;
					nextPhase <= init0;		--load next pt iteration for next round
				else
					nextPhase <= complete5;
				end if;
			when complete5 =>			 --needs condition to procced to request or next round
				load_done <= '1';
				counter <= 0;
				if(control_reg = X"01") then  --await command from NIOS II to begin another encrytion
					nextPhase <= request_Pt;
				else
					nextPhase <= complete5;
				end if;
			end case;
	end process;

	---begin combinational logic and register transfer

	---if first round, request plaintext and key from LFSR and NIOS II respectively



	---load plaintext

	plaintext_input <= plaintext when clk = '1' and clk'event and load_plaintext = '1';

	--key

    -- status register control

	status_reg <= 	X"00" when clk = '1' and clk'event and load_ptrequest = '1' 	else
					X"01" when clk = '1' and clk'event and load_plaintext = '1' 	else
					X"02" when clk = '1' and clk'event and load_keyRequest = '1'	else
					X"04" when clk = '1' and clk'event and round0addRound = '1' 	else
					--X"14" when clk = '1' and clk'event and waitSig = '1'			else
					X"08" when clk = '1' and clk'event and load_subBytes = '1' 		else
					X"10" when clk = '1' and clk'event and load_shiftRows = '1' 	else
					X"20" when clk = '1' and clk'event and load_mixCol = '1' 		else
					X"40" when clk = '1' and clk'event and load_addRoundKey = '1' 	else
					X"80" when clk = '1' and clk'event and load_done = '1';
    
					
	AddRoundKeyResult <= addRoundKey(roundKey(0), plaintext_input) when clk = '1' and clk'event and round0addRound = '1' else
						 addRoundKey(roundKey(10), shiftRowResult) when clk = '1' and clk'event and load_addRoundKey = '1' and counter = 10 else
						 addRoundKey(roundKey(counter), mixColumnResult) when clk = '1' and clk'event and load_addRoundKey = '1' and counter /= 10;
					  
	subByteResult <= subBytes(AddRoundKeyResult) when clk = '1' and clk'event and load_subBytes = '1';
	
	shiftRowResult <= shiftRows(subByteResult) when clk = '1' and clk'event and	load_shiftRows = '1';
										
	mixColumnResult <= mixColumns(shiftRowResult) when clk = '1' and clk'event and load_mixCol = '1'; 
	
	cipherText <= AddRoundKeyResult when clk = '1' and clk'event and load_done = '1'; 

end control;