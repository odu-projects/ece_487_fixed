library ieee;
use ieee.NUMERIC_STD.all;
use ieee.std_logic_1164.all;
library AES_128_832019;
use AES_128_832019.LUT_package.all;
use aes_test.all;

	-- Add your library and packages declaration here ...

entity aes_top_tb is
	-- Generic declarations of the tested unit
		generic(
		key_size : INTEGER := 128 );
end aes_top_tb;

architecture TB_ARCHITECTURE of aes_top_tb is
	-- Component declaration of the tested unit
	component aes_top
		generic(
		key_size : INTEGER := 128 );
	port(
		key : in STD_LOGIC_VECTOR(key_size-1 downto 0);
		plainText : in STD_LOGIC_VECTOR(127 downto 0);
		clk : in STD_LOGIC;
		control_reg : in STD_LOGIC_VECTOR(7 downto 0);
		cipherText : out STD_LOGIC_VECTOR(127 downto 0);
		status_reg : out STD_LOGIC_VECTOR(7 downto 0);
		pt_request : out STD_LOGIC;
		pt_status : in STD_LOGIC;
		busy : out STD_LOGIC );
	end component;

	-- Stimulus signals - signals mapped to the input and inout ports of tested entity
	signal key : STD_LOGIC_VECTOR(key_size-1 downto 0);
	signal plainText : STD_LOGIC_VECTOR(127 downto 0);
	signal clk : STD_LOGIC;
	signal control_reg : STD_LOGIC_VECTOR(7 downto 0);
	signal pt_status : STD_LOGIC;
	-- Observed signals - signals mapped to the output ports of tested entity
	signal cipherText : STD_LOGIC_VECTOR(127 downto 0);
	signal status_reg : STD_LOGIC_VECTOR(7 downto 0);
	signal pt_request : STD_LOGIC;
	signal busy : STD_LOGIC;
	signal simulationActive: boolean:= true;
	-- Add your code here ...

begin

	-- Unit Under Test port map
	UUT : aes_top
		generic map (
			key_size => key_size
		)

		port map (
			key => key,
			plainText => plainText,
			clk => clk,
			control_reg => control_reg,
			cipherText => cipherText,
			status_reg => status_reg,
			pt_request => pt_request,
			pt_status => pt_status,
			busy => busy
		);

	-- Add your stimulus here ...
	
	process
	begin
		while(simulationActive) loop
			clk <= '1';
			wait for 10 ns;
			clk <= '0';
			wait for 10 ns;
		end loop;
	end process;

	process
	begin
		for i in tests' range loop
			plainText <= tests(i).plain;
			wait for 20 ns;
			pt_status <= '0';  
			wait for 20 ns;
			pt_status <= '1'; 
			control_reg	<= X"02";
			wait for 1800 ns;
			control_reg	<= X"01";
			wait for 20 ns;
			assert cipherText = tests(i).cipher report "Incorrect ciphertext." severity ERROR;
		end loop;
		wait for 20 ns;
		report "Simulation complete." severity FAILURE;
		simulationActive <= false;
	end process;

end TB_ARCHITECTURE;

configuration TESTBENCH_FOR_aes_top of aes_top_tb is
	for TB_ARCHITECTURE
		for UUT : aes_top
			use entity work.aes_top(control);
		end for;
	end for;
end TESTBENCH_FOR_aes_top;

