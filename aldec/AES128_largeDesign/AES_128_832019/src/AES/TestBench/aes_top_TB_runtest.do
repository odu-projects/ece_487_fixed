SetActiveLib -work
comp -include "$dsn\src\LUTs\LUT_package.vhd" 
comp -include "$dsn\src\AES\AES_entity.vhd" 
comp -include "$dsn\src\TestBench\aes_top_TB.vhd" 
asim +access +r TESTBENCH_FOR_aes_top 
wave 
wave -noreg key
wave -noreg plainText
wave -noreg clk
wave -noreg control_reg
wave -noreg cipherText
wave -noreg status_reg
wave -noreg pt_request
wave -noreg pt_status
wave -noreg busy
# The following lines can be used for timing simulation
# acom <backannotated_vhdl_file_name>
# comp -include "$dsn\src\TestBench\aes_top_TB_tim_cfg.vhd" 
# asim +access +r TIMING_FOR_aes_top 
