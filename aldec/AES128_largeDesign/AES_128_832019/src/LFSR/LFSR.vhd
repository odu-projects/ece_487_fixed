library IEEE;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

--128 bit linear feedback shift register to produce plaintexts or keys (former for now)
-- seed can be entered in NIOS II code

entity LFSR_128 is
	port( 	clk:			in std_logic;
			control_reg: 	in std_logic_vector(7 downto 0);
			status_reg:		out std_logic_vector(7 downto 0);
			seed_reg3:		in std_logic_vector(31 downto 0);  --pio cores can only be up to 32 bits, these will be loaded in parallel
			seed_reg2:		in std_logic_vector(31 downto 0);
			seed_reg1:		in std_logic_vector(31 downto 0);
			seed_reg0:		in std_logic_vector(31 downto 0);
			pt_out:			out std_logic_vector(127 downto 0);
			done:			out std_logic
		);
end entity LFSR_128;

architecture lfsr of lfsr_128 is

	type state_type is (s0, s1, s2, s3, s4, s5);
	signal state:		state_type:= s0;
	signal nextState:	state_type;

	signal transform:								std_logic_vector(127 downto 0 );
	signal input:									std_logic_vector(127 downto 0 );
	signal load_seed:								std_logic;
	signal load_transform:							std_logic;
	signal load_plaintext:							std_logic;
	signal load_done:								std_logic;
	signal load_addBit:								std_logic;
	signal ready:									std_logic;
	signal error_sig:								std_logic;
	signal new_bit:									std_logic;

begin

	process(clk)								 --really only need shift and reset control inputs
	begin
		if (clk = '1' and clk'event) then
			state <= nextState;
		end if;
	end process;

	process(state, control_reg)
	begin

		load_transform 		<= '0';
		load_plaintext 		<= '0';
		load_done			<= '0';
		ready				<= '0';
		error_sig			<= '0';
		load_addBit			<= '0';

		case state is
			when s0 =>
				ready <= '1';
				if(control_reg = X"00") then	--0x00 will be hold/reset command
					nextState <= s0;
				else
					nextState <= s1;
				end if;
			when s1 =>
				if(control_reg = X"00") then
					nextState <= s0;
				else
					nextState <= s2;
				end if;
			when s2 =>
				load_addBit <= '1';
				nextState <= s3;
			when s3 =>
				load_transform <= '1';
				if(control_reg = X"00") then
					nextState <= s0;
				else
					nextState <= s4;
				end if;
			when s4 =>
				load_plaintext <= '1';
				nextState <= s5;
			when s5 =>
				load_done <= '1';		   --done
				if(control_reg = X"00") then --reset and load new seeds
					nextState <= s0;
				else
					nextState <= s1;		 --continue crunching
				end if;
			when others =>
				error_sig <= '1';
				nextState <= s0;
		end case;
	end process;

	input   <=	 (seed_reg3 & seed_reg2 & seed_reg1 & seed_reg0) when ((clk = '1' and clk'event) and load_seed = '1')
		else	transform when (clk'event and clk = '1');	   --when load seed -> '0', crunch!

	--- load seeds or continue crunching

	input   <=	 (seed_reg3 & seed_reg2 & seed_reg1 & seed_reg0) when ((clk = '1' and clk'event) and load_seed = '1')
		else	transform when (clk'event and clk = '1');	   --when load seed -> '0', crunch!

	--get newBit

	new_bit 	<=	(input(127) xor input(126) xor input(125)) xor input(120) when ((clk = '1' and clk'event) and load_addBit = '1');

	transform	<=	input(126 downto 0) & new_bit when (clk = '1' and clk'event) and load_transform = '1';

	pt_out		<= 	transform when (clk = '1' and clk'event) and load_plaintext = '1';


	---load done -- pass status signals up to NIOS II

	status_reg  <= 	X"01" when ready = '1' and (clk = '1' and clk'event) else
					X"02" when load_seed = '1' and (clk = '1' and clk'event) else
					X"04" when load_transform = '1' and (clk = '1' and clk'event) else
				    X"08" when load_plaintext = '1' and (clk = '1' and clk'event) else
					X"10" when load_done = '1' and (clk = '1' and clk'event) else
					X"20" when error_sig = '1' and (clk = '1' and clk'event);

	load_seed <= '1' when control_reg = X"01" else '0';

	done <= load_done when clk = '1' and clk'event;

end lfsr;