library ieee;
use ieee.NUMERIC_STD.all;
use ieee.std_logic_1164.all;

	-- Add your library and packages declaration here ...

entity lfsr_128_tb is
end lfsr_128_tb;

architecture TB_ARCHITECTURE of lfsr_128_tb is
	-- Component declaration of the tested unit
	component lfsr_128
	port(
		clk : in STD_LOGIC;
		control_reg : in STD_LOGIC_VECTOR(7 downto 0);
		status_reg : out STD_LOGIC_VECTOR(7 downto 0);
		seed_reg3 : in STD_LOGIC_VECTOR(31 downto 0);
		seed_reg2 : in STD_LOGIC_VECTOR(31 downto 0);
		seed_reg1 : in STD_LOGIC_VECTOR(31 downto 0);
		seed_reg0 : in STD_LOGIC_VECTOR(31 downto 0);
		pt_out : out STD_LOGIC_VECTOR(127 downto 0) );
	end component;

	-- Stimulus signals - signals mapped to the input and inout ports of tested entity
	signal clk : STD_LOGIC;
	signal control_reg : STD_LOGIC_VECTOR(7 downto 0);
	signal seed_reg3 : STD_LOGIC_VECTOR(31 downto 0);
	signal seed_reg2 : STD_LOGIC_VECTOR(31 downto 0);
	signal seed_reg1 : STD_LOGIC_VECTOR(31 downto 0);
	signal seed_reg0 : STD_LOGIC_VECTOR(31 downto 0);
	-- Observed signals - signals mapped to the output ports of tested entity
	signal status_reg : STD_LOGIC_VECTOR(7 downto 0);
	signal pt_out : STD_LOGIC_VECTOR(127 downto 0);

	-- Add your code here ...

	signal simulationActive: boolean:= true;

begin

	-- Unit Under Test port map
	UUT : lfsr_128
		port map (
			clk => clk,
			control_reg => control_reg,
			status_reg => status_reg,
			seed_reg3 => seed_reg3,
			seed_reg2 => seed_reg2,
			seed_reg1 => seed_reg1,
			seed_reg0 => seed_reg0,
			pt_out => pt_out
		);

	-- Add your stimulus here ...

	process
	begin
		while(simulationActive) loop
			clk <= '1';
			wait for 10 ns;
			clk <= '0';
			wait for 10 ns;
		end loop;
		wait on simulationActive;
	end process;

	process
	begin
		seed_reg0 <= X"12341234";	  --load seed parts in parallel
		seed_reg1 <= X"ABCDABCD";
		seed_reg2 <= X"CDEFCDEF";
		seed_reg3 <= X"12345678";
		control_reg <= X"01";
		wait for 80 ns;
		--concatenate
		--load transform
		control_reg <= X"02"; --continue shifting indefinitely	-- makes transform register uninitialized for first round!
		wait for 20 ns;
		--load plaintext
		wait for 20 ns;
		--load done, shift again;
		wait for 400 ns;
		seed_reg0 <= X"12551255";	  --load seed parts in parallel
		seed_reg1 <= X"AB55AB55";
		seed_reg2 <= X"CD55CD55";
		seed_reg3 <= X"12555578";
		control_reg <= X"01";
		wait for 60 ns;
		simulationActive <= false;			  --end simulation
		wait;
	end process;

end TB_ARCHITECTURE;

configuration TESTBENCH_FOR_lfsr_128 of lfsr_128_tb is
	for TB_ARCHITECTURE
		for UUT : lfsr_128
			use entity work.lfsr_128(lfsr);
		end for;
	end for;
end TESTBENCH_FOR_lfsr_128;



