SetActiveLib -work
comp -include "$dsn\src\LFSR\LFSR.vhd" 
comp -include "$dsn\src\TestBench\lfsr_128_TB.vhd" 
asim +access +r TESTBENCH_FOR_lfsr_128 
wave 
wave -noreg clk
wave -noreg control_reg
wave -noreg status_reg
wave -noreg seed_reg3
wave -noreg seed_reg2
wave -noreg seed_reg1
wave -noreg seed_reg0
wave -noreg pt_out
wave -noreg done
# The following lines can be used for timing simulation
# acom <backannotated_vhdl_file_name>
# comp -include "$dsn\src\TestBench\lfsr_128_TB_tim_cfg.vhd" 
# asim +access +r TIMING_FOR_lfsr_128 
