written by Micah Lehman 8/21/2019 3:59 pm
updated by Micah Lehman 11/9/2019 2:22 pm

# Design summary and details

## preliminary notes: 

key expansion is not implemented, this is a known plaintext attack, so key and appropriate
key transformations will be supplied at correct time. A handful of different keys and
their transformatins will be supplied for thoroughness of attack proof. Keys will be 
stored in container in NIOS II C code.

This design will be interrupt driven. This will keep it simple, and although an [RTOS
is available for NIOS II](https://www.intel.com/content/dam/www/programmable/us/en/pdfs/literature/tt/tt_nios2_microc_osii_tutorial.pdf)
it won't be used here due to time contraints. 

This design consists of 4 major modules:
	- AES encryption module
	- Linear Feedback Shift Register
	- a NIOS II soft-core processor (not within this aldec design but part of the final quartus project)
	- a module to write information to the LCD screen, this is tentative but this way Ill be able to cram more information
	onto a visible output, as opposed to just using the HEX displays. If I can figure it out, I may used an [I2C master core](https://www.intel.com/content/dam/www/programmable/us/en/pdfs/literature/ug/ug_embedded_ip.pdf#page=167)
	to control the LCD if its got an I2C i/o expander backpack.
	These will be instantiated in a top level entity to provide access to input and other peripherals

## module signal summary:
### aes
	- key input (256 bits- smaller keys will be pre-padded and key decoded for use in AES)- if time permits, a handful of key sizes will
	  be used for thoroughness, key schedule i/o will be left for future implementation
	- key size is paramaterized and must be set in quartus before synthesis! (assignments > settings > default parameters...remember
	  only 3 key sizes are permitted and system designed to suspend if invalid key size detected)-- this is tentative
	- clock
	- 8-bit control register (controlled by NIOS: 0x01 = start, 0x02 to suspend, 0x04 to reset)
	- ciphertext output (always 128 bits)
	- 8-bit status register -- need to settle on commands
	- 1 bit key request signal - key expansion not implemented- likely will generate NIOS II interrupt to acquire key at correct time
	- May fold this into status register with associated bit interrupt mask for timely request

### LFSR
	- 8 bit control register
	- 8 bit status register (each state causes a value to be passed to register- this is for later flexibility
	  as not all of them need to be used for operations in NIOS II)
	- clock
	- 4 seed input registers (each pio can only be 32 bits maximum)- might change to one later and use interrupts to load
	  seed values if seems reasonable and time permits
	- plaintext_out register (to be loaded into encryption module)


### NIOS II
	- nothing here yet

### LCD Controller
	- nothing here yet

## overall design procedure:
	1. LFSR will generate a plaintext, finish, then suspend (controlled from nios II, which will receive an interrupt signal at
	   plaintext generation) and generate an interrupt via the PIO core.
	2. The plain text will be loaded into the AES module which will request keys at appropriate times for each round,
	   since key expansion doesn't need to be implemented in this project, a pre-filled container of keys will supply key values
	   to encryption rounds at appropriate times via AES key request, which is a specific value in AES status register (see above)
	   which will generate an interrupt in the NIOS II which will supply the key value.  
	3. Upon generation of a ciphertext, AES will send a done signal via another specific value of the status register, which will  
	   generate another NIOS II interrupt which suspends AES operations  
	4. This will cause the NIOS II processor to display pertinent information on an LCD (I2C or SPI vira serial or just plain parallel)  
	   transmission is suspended, and the NIOS II signals the LFSR to generate another plaintext.


## NOTE: 
It remains to be seen which board the final design will be implemented on. It will either be the current board (de2-115), a
DE0-nano, or the waveshare core board, which is just a 10k alm cyclone fpga on a board with JTAG and some GPIO.
