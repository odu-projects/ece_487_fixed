aes_f2019 is a simple but a complete AES-128 implementation. The design code is similar to the simplified_32 bit AES.

The following pins / peripherals are used in the design:

	SW[0] (PIN-AB28)					- A value of '1' starts the encryption process. '0' puts the process in a wait state.
	SW[2:1] (PIN_AC27 AND PIN_AC28) 	- Used to control which part of the ciphertext is displayed on the seven segment displays.
										  '11' displays bits[127:96], '10' displays bits[95:64], '01' displays bits[32:64], '00' displays bits[31:0].									  
	CLOCK_50 (PIN_Y2) 					- Use as input clock to the PLL.
	TRIGGER (PIN_AG26)					- Use as marker to signal oscilloscope to start data acquisition. The same pin is used for the 32-bit design.

The initial plaintext seed and the encryption keys for all 10 encryption rounds are hard-coded in the design for simplicity.
The design is currently configured to perform 32 encryption cycles of the same plaintext before encrypting a new plaintext for oscilloscope averaging mode.
The code also includes several comments to make it easier to follow the design flow and which lines/variables can be changed as desired.

