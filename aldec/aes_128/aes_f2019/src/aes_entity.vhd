library IEEE;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.LUT_package.all;

entity aes_top is
	GENERIC	(	key_size:				in  integer:= 128);
	port 	(	CLOCK_50:			 	in 	std_logic;
				KEY:					in  std_logic_vector (4 downto 0);
				SW:						in  std_logic_vector (17 downto 0);
				LEDR:					out std_logic_vector( 17 downto 0 );
				LEDG: 					out std_logic_vector( 8 downto 0 );
				TRIGGER:				out std_logic;
				HEX0:					out std_logic_vector (6 downto 0);
				HEX1:					out std_logic_vector (6 downto 0);
				HEX2:					out std_logic_vector (6 downto 0);
				HEX3:					out std_logic_vector (6 downto 0);
				HEX4:					out std_logic_vector (6 downto 0);
				HEX5:					out std_logic_vector (6 downto 0);
				HEX6:					out std_logic_vector (6 downto 0);
				HEX7:					out std_logic_vector (6 downto 0)
			);

end entity aes_top;

architecture control of aes_top is	

component seven_segment is  
	port(ciphertext: in unsigned(127 downto 0);
		 sw_21: in std_logic_vector (1 downto 0);
		 SS_0: out std_logic_vector(6 downto 0);
		 SS_1: out std_logic_vector(6 downto 0);
		 SS_2: out std_logic_vector(6 downto 0);
		 SS_3: out std_logic_vector(6 downto 0);
		 SS_4: out std_logic_vector(6 downto 0);
		 SS_5: out std_logic_vector(6 downto 0);
		 SS_6: out std_logic_vector(6 downto 0);
		 SS_7: out std_logic_vector(6 downto 0)); 
end component;

component pll IS
	PORT
	(
		areset		: IN STD_LOGIC  := '0';
		inclk0		: IN STD_LOGIC  := '0';
		c0		: OUT STD_LOGIC ;
		c1		: OUT STD_LOGIC ;
		c2		: OUT STD_LOGIC ;
		c3		: OUT STD_LOGIC 
	);
END component;


type state is (ready, start, request_pt, request_key, init0, initAddRound, subBytes1, shiftRows2, mixCol3, addRoundKey4, complete5, pt_wait, key_wait, rcv_key, update, wait_next, delay);

signal phase, nextPhase:			 		state;
signal plainText_input, cipherText:		 	std_logic_vector(127 downto 0);
signal state_array:							std_logic_vector(127 downto 0);
signal load_plaintext, load_key, init:  	std_logic;
signal load_subBytes, load_shiftRows:		std_logic;
signal load_mixCol, load_addRoundKey:		std_logic;
signal load_done:							std_logic;
signal load_ptRequest, load_keyRequest:		std_logic;
signal round_count:							integer;
signal counter:								integer:= 0;
signal cycle:								std_logic;
signal round0addRound:						std_logic;
signal key_request, pt_request:							std_logic;
signal bit0:								std_logic;
signal subByteResult, shiftRowResult:		std_logic_vector(127 downto 0);
signal mixColumnResult, AddRoundKeyResult:	std_logic_vector(127 downto 0);	 
signal plainText:							std_logic_vector(127 downto 0) := X"12341234ABCDABCDCDEFCDEF12345678";	
signal clk, inc_delay:						std_logic;
signal delayCount:							integer := 0;	  
signal reset_count, update_display:			std_logic; 
signal pulse_trigger:						std_logic;
signal clock_50Mhz, clock_5MHz, clock_1MHz, clock_100KHz: std_logic;

constant roundKey: LUT_key:=(
	X"00000000000000000000000000000000",
	X"62636363626363636263636362636363",
	X"9B9898C9F9FBFBAA9B9898C9F9FBFBAA",
	X"90973450696CCFFAF2F457330B0FAC99",
	X"EE06DA7B876A1581759E42B27E91EE2B",
	X"7F2E2B88F8443E098DDA7CBBF34B9290",
	X"EC614B851425758C99FF09376AB49BA7",
	X"217517873550620BACAF6B3CC61BF09B",
	X"0EF903333BA9613897060A04511DFA9F",
	X"B1D4D8E28A7DB9DA1D7BB3DE4C664941",
	X"B4EF5BCB3E92E21123E951CF6F8F188E"
);

begin

	round_count <= 	10 when key_size = 128 else
					12 when key_size = 192 else
					14 when key_size = 256;

	process(clk)			--state control
	begin
		if(clk = '1' and clk'event) then
			phase <= nextPhase;
		end if;
	end process;	
	
	process(clk)
	begin
		if clk'event and clk = '1' then
			if reset_count = '1' then
				delayCount <= 0;
			elsif inc_delay = '1' then
				delayCount <= delayCount + 1;
			end if;
		end if;
	end process;
	
	process(clk)
	begin
		if clk'event and clk = '1' then
			if load_done = '1' then
				counter <= 0;
			elsif load_subBytes = '1' then
				counter <= counter + 1;
			end if;
		end if;
	end process;

	process(phase, SW) 			--encryption control
	begin

		load_plaintext 		<= '0';
		load_key			<= '0';				   
		init				<= '0';
		load_subBytes 		<= '0';
		load_shiftRows		<= '0';
		load_mixCol			<= '0';
		load_addRoundKey	<= '0';
		load_done			<= '0';
		load_keyRequest		<= '0';
		load_ptRequest		<= '0';
		round0addRound		<= '0';
		pt_request			<= '0';		  --goes to LFSR
		key_request			<= '0';		  --goes to NIOS II
		inc_delay 			<= '0';
		update_display		<= '0';
		pulse_trigger		<= '0';
		reset_count 		<= '0';
		case phase is
			when ready =>
				if (SW(0) = '1') then
					nextPhase <= start;
				else
					nextPhase <= ready;
				end if;
			when start =>
				pulse_trigger <= '1';
				nextPhase <= request_pt;
			when request_pt =>
				load_ptRequest <= '1';
				nextPhase <= pt_wait;
			when pt_wait =>
				pt_request <= '1';
		--		if(pt_status = '1') then
					nextPhase <= init0;
		--		else
		--			nextPhase <= pt_wait;
		--		end if;
			when init0 =>
				load_plaintext <= '1';
				nextPhase <= request_key;
			when request_key =>
				load_keyRequest <= '1';
				nextPhase <= key_wait;
			when key_wait =>
				key_request <= '1';
		--		if(control_reg = X"02") then
					nextPhase <= rcv_key;
		--		else
		--			nextPhase <= key_wait;
		--		end if;
			when rcv_key =>
				load_key <= '1';
				if(counter = 0) then
					nextPhase <= initAddRound;
				else
					nextPhase <= subBytes1;
				end if;
			when initAddRound =>
				round0addRound <= '1';
				nextPhase <= subBytes1;
			when subBytes1 =>
		--		counter <= counter + 1;
				load_subBytes <= '1';
				nextPhase <= shiftRows2;
			when shiftRows2 =>
				load_shiftRows <= '1';
				if(counter = round_count) then
					nextPhase <= addRoundkey4;
				else
					nextPhase <= mixCol3;
				end if;
			when mixCol3 =>					--might break up into more than one cycle
				load_mixCol <= '1';
				nextPhase <= addRoundKey4;
			when addRoundKey4 =>
				load_addRoundKey <= '1';
				if(counter < round_count) then
					nextPhase <= init0;		--load next pt iteration for next round
				else
					nextPhase <= complete5;
				end if;
			when complete5 =>			 --needs condition to procced to request or next round
				load_done <= '1';
	--			counter <= 0;
	--			if(control_reg = X"01") then  --await command from NIOS II to begin another encrytion
	--				nextPhase <= request_Pt;
	--			else
					nextPhase <= update;
	--			end if;	
			when update =>
				update_display <= '1';
				nextPhase <= wait_next;
				
			when wait_next =>
				if delayCount = 10 then				  --delay to transmit data to oscope
					reset_count <= '1';
					nextPhase <= ready;
				else 
					inc_delay <= '1';
					nextPhase <= delay;
				end if;
			
			when delay =>
				inc_delay <= '1';
				nextPhase <= wait_next;
			
			end case;
	end process;

	---begin combinational logic and register transfer

	-- generate plaintext
	bit0 <= plaintext(127) xor plaintext(126) xor plaintext(125) xor plaintext(120) when clk = '1' and clk'event and load_ptRequest = '1'; 
	
	plaintext <= plaintext(126 downto 0) & bit0 when clk = '1' and clk'event and pt_request = '1';

	---load plaintext
	plaintext_input <= plaintext when clk = '1' and clk'event and load_plaintext = '1';

    -- trigger acquisition
	trigger <= '1' when  clk = '1' and clk'event and pulse_trigger = '1' else '0' when  clk = '1' and clk'event and pulse_trigger = '0';

--	status_reg <= 	X"00" when clk = '1' and clk'event and load_ptrequest = '1' 	else
--					X"01" when clk = '1' and clk'event and load_plaintext = '1' 	else
--					X"02" when clk = '1' and clk'event and load_keyRequest = '1'	else
--					X"04" when clk = '1' and clk'event and round0addRound = '1' 	else
--					X"08" when clk = '1' and clk'event and load_subBytes = '1' 		else
--					X"10" when clk = '1' and clk'event and load_shiftRows = '1' 	else
--					X"20" when clk = '1' and clk'event and load_mixCol = '1' 		else
--					X"40" when clk = '1' and clk'event and load_addRoundKey = '1' 	else
--					X"80" when clk = '1' and clk'event and load_done = '1';
    					
	AddRoundKeyResult <= addRoundKey(roundKey(0), plaintext_input) when clk = '1' and clk'event and round0addRound = '1' else
						 addRoundKey(roundKey(10), shiftRowResult) when clk = '1' and clk'event and load_addRoundKey = '1' and counter = 10 else
						 addRoundKey(roundKey(counter), mixColumnResult) when clk = '1' and clk'event and load_addRoundKey = '1' and counter /= 10;
					  
	subByteResult <= subBytes(AddRoundKeyResult) when clk = '1' and clk'event and load_subBytes = '1';
	
	shiftRowResult <= shiftRows(subByteResult) when clk = '1' and clk'event and	load_shiftRows = '1';
										
	mixColumnResult <= mixColumns(shiftRowResult) when clk = '1' and clk'event and load_mixCol = '1'; 
	
	cipherText <= AddRoundKeyResult when clk = '1' and clk'event and load_done = '1';
	
	hex: seven_segment  
	port map
	(
		ciphertext => unsigned(cipherText),
		 sw_21 => SW (2 downto 1),
		 SS_0 => HEX0,
		 SS_1 => HEX1,
		 SS_2 => HEX2,
		 SS_3 => HEX3,
		 SS_4 => HEX4,
		 SS_5 => HEX5,
		 SS_6 => HEX6,
		 SS_7 => HEX7
	);
	
	pll_inst : pll 
	PORT MAP
	(
		areset	 => '0',
		inclk0	 => clock_50,
		c0	 => clock_50MHz,
		c1	 => clock_5MHz,
		c2	 => clk,
		c3	 => clock_100KHz
	);

end control;