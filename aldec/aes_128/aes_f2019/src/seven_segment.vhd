library ieee; 
use ieee.std_logic_1164.all; 
use ieee.numeric_std.all; 

entity seven_segment is  
	port(ciphertext: in unsigned(127 downto 0);
		 sw_21: in std_logic_vector (1 downto 0);
		 SS_0: out std_logic_vector(6 downto 0);
		 SS_1: out std_logic_vector(6 downto 0);
		 SS_2: out std_logic_vector(6 downto 0);
		 SS_3: out std_logic_vector(6 downto 0);
		 SS_4: out std_logic_vector(6 downto 0);
		 SS_5: out std_logic_vector(6 downto 0);
		 SS_6: out std_logic_vector(6 downto 0);
		 SS_7: out std_logic_vector(6 downto 0)); 
end entity seven_segment; 

architecture dataflow of seven_segment is 

type Hex_to_seven is array ( 0 to 15) of std_logic_vector( 0 to 6);
constant seven_segment: Hex_to_seven:=("0000001", "1001111", "0010010", "0000110",
												 "1001100", "0100100", "0100000", "0001111",
												 "0000000", "0000100", "0001000", "1100000",
												 "0110001", "1000010", "0110000", "0111000"
												 );	 
signal show: unsigned (31 downto 0);												

begin  
	process(ciphertext, sw_21) 	 
	begin  
		case sw_21 is 
			when "00" => 
				show <= ciphertext(31 downto 0);
			when "01" => 
				show <= ciphertext(63 downto 32);
			when "10" => 
				show <= ciphertext(95 downto 64);
			when "11" => 
				show <= ciphertext(127 downto 96);
			when others =>
				show <= X"00000000";
		end case;
	end process; 
	
	SS_0 <= seven_segment(to_integer(show(3 downto 0)));
	SS_1 <= seven_segment(to_integer(show(7 downto 4)));
	SS_2 <=seven_segment(to_integer(show(11 downto 8)));
	SS_3 <=seven_segment(to_integer(show(15 downto 12)));
	SS_4 <=seven_segment(to_integer(show(19 downto 16)));
	SS_5 <=seven_segment(to_integer(show(23 downto 20)));
	SS_6 <= seven_segment(to_integer(show(27 downto 24)));
	SS_7 <= seven_segment(to_integer(show(31 downto 28)));	
	
end architecture dataflow;