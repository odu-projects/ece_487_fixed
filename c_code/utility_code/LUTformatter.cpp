#include <stdio.h>
#include <string>
#include <regex>
#include <fstream>
#include <iostream>
#include <sstream>

//c++ program, no need to be c

int main(int argc, char** argv)
{
	if(argc > 3)
	{
		std::cout << "Only pass two argument to this program.\n";
		std::cout << "This program's purpose is to convert the \n";
		std::cout << "c-style hex values of the LUTs to values \n";
		std::cout << "usable by VHDL example: 0x04 to X\"04\"\n";
		return 0;
	}
	else if(argc <= 2)
	{
		std::cout <<"Program requires two cmd line inputs of [inputFile name] and [outputFile name]!\n";
		return 0;
	}
		
	std::string inputFileName;
	inputFileName = argv[1];

	std::string outputFileName;
	outputFileName = argv[2];

	std::ifstream inputFile;
	inputFile.open(inputFileName, std::ifstream::in );

	if(!inputFile)
	{
		std::cerr << "input file doesn't exist!\n";
		return 0;
	}

	std::string text_contents, formatted_output1, formatted_output2, line;

	std::ostringstream input_stream;

	while (getline(inputFile, line))
	{
		input_stream << line << "\n";
	}

	text_contents = input_stream.str();

	std::regex c2vhdl_hex_begin ("0x"); //0x -> X"
	std::regex c2vhdl_hex_end (",");	// , -> ", 

	std::regex_replace(std::back_inserter(formatted_output1), text_contents.begin(), text_contents.end(), c2vhdl_hex_begin, "X\"" );
	std::regex_replace(std::back_inserter(formatted_output2), formatted_output1.begin(), formatted_output1.end(), c2vhdl_hex_end, "\", " );

	inputFile.close();

	std::ofstream outputFile (outputFileName, std::ofstream::out);
	outputFile << formatted_output2;

	outputFile.close();

	return 0;
}