# C Code README

This directory contains helpful code not directly used in the project in any way. There are some AES LUTs, some code to generate values for the LFSR (additionally Carlo has created code in VHDL with a nice testbench to do the same thing), and ine utility code folder, there is a program that takes c-style LUTs of hex values and converts them to VHDL style LUTs using mostly RegEx. 

The code that generates values for the LFSR is based on two 64bit ints as there is no convenient 128bit int types in the C++ std lib. 

Micah Lehman 11/17/2019
