#include <string>
#include <fstream>
#include <iostream>
#include <iomanip>
#include <sstream>
#include <stdint.h>
#include <list>
#include <utility>
#include <fstream>

#define AND_MASK 0x8000000000000000

int main(int argc, char** argv)
{
	if(argc != 3)
	{
		std::cout << "pass 2 arguments: output text file name, # of output values.\n";
		std::cout << "This program will generate test vectors for \n";
		std::cout << "checking your Linear Feedback Shift Register.\n";
		std::cout << "Right now it will only check a 4 tap, 128 bit LFSR.\n";
		return 0;
	}

	std::pair<uint64_t, uint64_t> reg128;

	std::list<std::pair<uint64_t, uint64_t> > values;

	int count = std::stoi(argv[2]);

	uint64_t shR0 = 0xABCDABCD12341234;
	uint64_t shR1 = 0x12345678CDEFCDEF;
	uint64_t shTmp, bit;

	for(int i = 0; i < count; i ++) //taps are 128, 127, 126, 121 (subtract one from each)
	{
		bit = (shR1 << 0) ^ (shR1 << 1) ^ (shR1 << 2) ^ (shR1 << 7); //make new bit
		bit = bit >> 63;						//make it the LSB
		shR1 = shR1 << 1; 						//shift left 1, toward MSB, lowest bit now zero, needs to be highest bit from ShR0
		shTmp = (shR0 & AND_MASK) >> 63; 		//shit temp till msb is lsb and everything is zero
		shR1 = shR1 | shTmp;					//OR high slice with new LSB for shifting across two 64 bit ints
		shR0 = (shR0 << 1) | bit;				//shift low slice and or new bit (or'ed with 0, so should be good)
		reg128 = std::make_pair(shR1, shR0); 	//put em together
		values.push_back(reg128);				//store em
	}

	std::string outputfilename;
	outputfilename = argv[1];

	std::ofstream outputFile (outputfilename, std::ofstream::out);

	std::ostringstream value_output;

	for(std::list<std::pair<uint64_t,uint64_t> >::iterator it = values.begin(); it!=values.end(); it++)
	{
		outputFile << std::setfill('0') << std::setw(16) << std::setbase(16) << (*it).first << (*it).second << "\n";
	}

	outputFile.close();
}