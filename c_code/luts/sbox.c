#include <stdio.h>
#include <stdint.h>

#define ROTL8(x , shift) ((uint8_t) ((x) << (shift)) | ((x) >> (8 - (shift))))

int main()
{
	uint8_t sbox[256];
	uint8_t p = 1, q = 1;

	do{
		p = p ^ (p << 1) ^ (p & 0x80 ? 0x1B : 0);

		q ^= q << 1;
		q ^= q << 2;
		q ^= q << 4;
		q ^= q & 0x80 ? 0x09 : 0;

		uint8_t xformed = q ^ ROTL8(q, 1) ^ ROTL8(q, 2) ^ ROTL8(q, 3) ^ ROTL8(q, 4);

		sbox[p] = xformed ^ 0x63;
	} while (p != 1);

	sbox[0] = 0x63;

	FILE *fp;
	fp = fopen("sbox.txt", "w");

	for(int i = 1; i <= 256; i++ )
		fprintf(fp, "X\"%X\", ", sbox[i - 1]);
}