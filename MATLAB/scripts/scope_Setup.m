%need sequence trigger, current varies too much vertically- need 
%trigger on digital signal and within some current range

record_length = 1000; %still need to change on lines 42 and 68

visaAddress = 'TCPIP::192.168.1.101::INSTR';

myScope = visa('tek', visaAddress);

myScope.InputBufferSize = record_length;

myFgen.ByteOrder = 'littleEndian';

set(myScope,'Timeout',60);

fopen(myScope);

%reset scope
fprintf(myScope, '*RST'); 

%turn off channel 1
fprintf(myScope, 'SEL:CH1 OFF'); 

fprintf(myScope, 'SEL:CH2 ON');         %turn on channel 2
fprintf(myScope, 'AUTOSET EXECUTE');    %engage autoset
fprintf(myScope, 'SEL:D15 ON');         %turn on Digital channel 15

fprintf(myScope, 'DIS:DIG:HEI LARGE');  %make digital chan display "tall"
fprintf(myScope, 'DIS:DIG:ACTIV ON');   %make sure digital signal monitoring ON

fprintf(myScope, 'DAT:SOU CH2');        %make channel 2 analog data source
fprintf(myScope, 'CH2:YUN "A"');        %change ch2 units to amps
fprintf(myScope, 'CH2:SCALE 1E-3');    %set scale to 10mA/div (maybe too fine)
fprintf(myScope, 'CH2:POS -2');         %set vertical position for 5 below center graticule
fprintf(myScope, 'CH2:OFFS 407.5E-3');    %add 350mA offset to get signal well in view4188E-4

fprintf(myScope, 'HOR:SCA 100E-6');      %horizontal scale is 20us = 2 clock cycles per graticule

fprintf(myScope, 'D15:POS -3.9');       %position digital channel

fprintf(myScope, 'HOR:RECO 1000');     %"record_length" data points
fprintf(myScope, 'HDR OFF');           %remove CURVE Header

%fprintf(myScope, 'ACQ:MOD HIR');        %turn on hires mode
fprintf(myScope, 'ACQ:MOD AVE');        %turn on hires mode
fprintf(myScope, 'ACQ:NUMAV 32');      %maximum averaging

xZeroStr = query(myScope, 'WFMO:XZE?');  %return trigger time as center x value (0)
xIncStr  = query(myScope, 'WFMO:XINC?'); %return x increment

xZero = str2double(xZeroStr);   %convert strings to numbers
xInc = str2double(xIncStr);

yZeroStr = query(myScope, 'WFMO:YZE?');     %return vertical offset of wvfrm
yMultStr  = query(myScope, 'WFMO:YMU?');    %return vertical scale multiplying factor

xVector = 1:record_length;

yZero = str2double(yZeroStr);
yMult = str2double(yMultStr);

fprintf(myScope, 'TRIG:A:TYPE LOGI');           %logic trigger type
fprintf(myScope, 'TRIG:A:LOGI:CLA LOGI');       %use logic vs setup/hold
fprintf(myScope, 'TRIG:A:LOGI:INP:D15 HIGH');   %trigger when digital channel 15 goes high
fprintf(myScope, 'TRIG:A:MODE NORM');           %trigger mode normal, vs untriggered roll

fprintf(myScope, 'DAT:START 1');
fprintf(myScope, 'DAT:STOP 1000');     %"record_length" data points

fprintf(myScope, 'WFMO:BN_F RP');       %binary format as unsigned integer
fprintf(myScope, 'WFMO:BYT_N 1');       %data points 1 byte wide

fprintf(myScope, 'APPL:TYP ACTONEV');   %set application type to act on event

fprintf(myScope, 'ACTONEV:EVENTTYP TRIG');      %create trigger 'event'
fprintf(myScope, 'ACTONEV:ACTION:SRQ:STATE 1'); %create 'trigger' service request, wont execute any commands until OPC cleared
% opc = query(myScope, '*OPC?')
% disp(opc)
fprintf(myScope, 'DESE 1');
fprintf(myScope, '*ESE 1');
fprintf(myScope, '*SRE 32');            %generate service request when ESB (event status bit) goes high
fprintf(myScope, 'ACTONEV:REPEATC 10000'); %op count
numTraces = 1;
dataDPA = zeros(5,record_length);

fileID = fopen('C:\Users\engadmin\Desktop\ece_487_fixed\MATLAB\scripts\data.txt','w');
formatSpec = '%0.4f\n ';
% 
while( numTraces < 10001 )                          %infinte loop, controlled by count
    
    if( query(myScope, '*OPC?') )
        
        disp(numTraces);                        % display current trace
     
        fprintf(myScope, 'CURVE?');

        rawData = binblockread(myScope, 'uint8'); %read raw data as 8-bit unsigned integers
        rowRawData = rawData .' ;                 %change it from a column to a row;

        yprocessData = yMult .* rowRawData;
        yprocessData = yZero + yprocessData;
    
        dataDPA(numTraces, :) = yprocessData;
   %      timeDPA(numTraces, :) = xprocessData;
        
        fprintf(fileID, formatSpec, dataDPA(numTraces,:)); % copies data to a text file
       
        
        numTraces = numTraces + 1; 
 
    end;

 end;
 


 
 fclose(fileID);   
 
    xprocessData = xInc .* ( xVector - 1 );
    xprocessData = xZero + xprocessData;

    plotMin = xprocessData(1);
    plotMax = xprocessData(record_length);

    line(xprocessData, dataDPA(1,:));
    grid on;
    grid minor;
    axis([plotMin plotMax 0.005 0.007]);
    figure;
    
    line(xprocessData, dataDPA(2,:));
    grid on;
    grid minor;
    axis([plotMin plotMax 0.005 0.007]);
    figure;
    
    line(xprocessData, dataDPA(3,:));
    grid on;
    grid minor;
    axis([plotMin plotMax 0.005 0.007]);
    figure;
    
    line(xprocessData, dataDPA(4,:));
    grid on;
    grid minor;
    axis([plotMin plotMax 0.005 0.007]);
    figure;
 
fclose(myScope);

%-------------------------------------------------------------
% perform DPA analysis here

% AES-128 sBox table in decimal format
sBox = [099 124 119 123 242 107 111 197 048 001 103 043 254 215 171 118
        202 130 201 125 250 089 071 240 173 212 162 175 156 164 114 192
        183 253 147 038 054 063 247 204 052 165 229 241 113 216 049 021
        004 199 035 195 024 150 005 154 007 018 128 226 235 039 178 117
        009 131 044 026 027 110 090 160 082 059 214 179 041 227 047 132
        083 209 000 237 032 252 177 091 106 203 190 057 074 076 088 207
        208 239 170 251 067 077 051 133 069 249 002 127 080 060 159 168
        081 163 064 143 146 157 056 245 188 182 218 033 016 255 243 210
        205 012 019 236 095 151 068 023 196 167 126 061 100 093 025 115
        096 129 079 220 034 042 144 136 070 238 184 020 222 094 011 219
        224 050 058 010 073 006 036 092 194 211 172 098 145 149 228 121
        231 200 055 109 141 213 078 169 108 086 244 234 101 122 174 008
        186 120 037 046 028 166 180 198 232 221 116 031 075 189 139 138
        112 062 181 102 072 003 246 014 097 053 087 185 134 193 029 158
        225 248 152 017 105 217 142 148 155 030 135 233 206 085 040 223
        140 161 137 013 191 230 066 104 065 153 045 015 176 084 187 022];
  
% correct is key is d4f0cbc3
%correct_key = [212  240 203 195];
correct_key = [000  000 000 000 000  000 000 000 000  000 000 000 000  000 000 000 ];
traceLength = 1000;         % record length / number of points per trace
samplePoints = 0:1:999;
numTraces = 10000;           % number of current trace collected
key_length = 16;             % key length in bytes. Use 16 for a 128-bit key
plaintext = zeros(numTraces-1,key_length); % initialize plaintext vector with all zeros
%traces = zeros(numTraces-1,traceLength);   % initialize trace vector with all zeros
traceAve = zeros(1,traceLength);        % holds the average of all traces collected

%fileID1 = fopen('data.txt', 'r');          %open file containing trace values in read mode
fileID2 = fopen('plaintext.txt', 'r');  % open plaintext file in read mode

for i = 1:numTraces-1
    text_in = fgets(fileID2, key_length * 8);
    [row, col] = sscanf(text_in, '%02x ', key_length);
    plaintext(i,:) = row;                               %load plaintexts
%    traces(i,:) = fscanf(fileID1, '%f', traceLength);   %load traces
end
%fclose(fileID1);
fclose(fileID2);


% calculate the average of all traces collected
 for i = 1:numTraces-1                                % 
    traceAve(1,:) = traceAve(1,:) + dataDPA(i,:);
 end
 traceAve(1,:) = traceAve(1,:) / (numTraces-1);
 
% preprocess traces by first subtracting the average of all traces
% collected from each trace then squaring the resulting data point
for i = 1:numTraces-1  
    dataDPA(i,:) = (dataDPA(i,:) - traceAve(1,:)) .^ 2.0;
end
 
% calculate standard deviation among of all traces at each sample point
sum = zeros(1,traceLength);
stdDev = zeros(1,traceLength);
for j = 1:numTraces-1
 %   for k = 1:traceLength
        sum(1,:) = sum(1,:) + ((dataDPA(j,:) - traceAve(1,:)) .^ 2.0);
  %  end   
end
stdDev(1,:) = sqrt(sum(1,:) / (numTraces-2));

len = 16; % same as key length. change to 16 for 128-bit AES
for byte_num = 1:key_length    % for guessing all key bytes
    
    diffTrace = zeros(256, traceLength);
    group0 = zeros(256,traceLength);        % group0 holds the sum of traces where LSB of selection function = 0
    group1 = zeros(256,traceLength);        % group1 holds the sum of traces where LSB of selection function = 1
    
 
    for keyGuess = 0:255    % compute difference trace for all possible key guesses

        % use to separate result of the binary selection function
        % LSB1 = number of traces with LSB = 1
        % LSB0 = number of traces with LSB = 0
        % initializing/resetting all values to zero
        
        LSB1 = 0;
        LSB0 = 0;

        for i = 1:numTraces-1                                % 
            byte3 = plaintext(i,byte_num);                   % get byte 3 value of each plaintexts
            sBoxValue = sBox(bitxor(byte3, keyGuess)+ 1);    % perform bitwise XOR of byte 3 and the key guess use the result as index to the Sbox table. Add 1 to access correct index                                              
            binSelectFunction = bitget(sBoxValue, 1);        % use LSB of sbox value as the binary selection function
            if binSelectFunction == 1                        % if LSB = 1, add trace to the group1 subset and increment LSB1 counter
                group1(keyGuess+1,:) = group1(keyGuess+1,:) + dataDPA(i,:);        
                LSB1 = LSB1 + 1;
            else                                             % if LSB = 0, add trace to the group0 subset and increment LSB0 counter
                group0(keyGuess+1,:) = group0(keyGuess+1,:) + dataDPA(i,:);
                LSB0 = LSB0 + 1;
            end                                             % LSB1 + LSB0 should be equal to the total number of traces - 1
        end
        group1(keyGuess+1,:) = (group1(keyGuess+1,:) ./ LSB1) .* 1e3;           % compute the point to point trace average of group1 and group0 subsets in mA
        group0(keyGuess+1,:) = (group0(keyGuess+1,:) ./ LSB0) .* 1e3;
        diffTrace(keyGuess+1,:) = ((group1(keyGuess+1,:) - group0(keyGuess+1,:))); %.* 1e3; % compute the  difference between the two trace subsets in uA
    end

    % divide each point in the differential trace by the standard deviation
    % of all traces at that point to normalize the trace. 
%     for k = 1:256
%         diffTrace(k,:) = (diffTrace(k,:) ./ stdDev(1,:));
%     end

% -------------------------------------------------------------------------------------------------------------------
    bestGuess = 0.0;
    x = 0.0;
    %key guess with highest peak is predicted as the correct key guess
    for i = 1:256
        x = max(diffTrace(i,:));
        if  x > bestGuess
            bestGuess = x;
            index = i;
        end;
    end;
    len = len - 1;
    fprintf('Best guess for byte %d is 0x%2.2X with max peak of %f uA \n', len, index-1, (bestGuess * 1e6));
    
    % plot correct key
    subplot(2,1,1);
    plot(samplePoints, (diffTrace(correct_key(byte_num)+1,:) .* 1e6));
    title(['Correct Key Byte = ', num2str(correct_key(byte_num), '%x')]); 
    xlabel('Sample points');
    ylabel('Current (uA)');
 %   ylim([-0.01 0.01]);

    % plotting best key guess
    subplot(2,1,2);
    plot(samplePoints, (diffTrace(index,:) .* 1e6));
    title(['Guess Key Byte = ', num2str(index-1, '%x')]);
    xlabel('Sample points');
    ylabel('Current (uA)');
 %   ylim([-0.01 0.01]);
    figure();
                
end;

%plotting average of all traces and standard deviation
subplot(2,1,1);
plot(samplePoints, traceAve(1,:));
title('Average of all traces collected');
xlabel('Sample points');
ylabel('Current (mA)');
subplot(2,1,2);
plot(samplePoints, stdDev);
title('Point by point standard deviation of all traces collected');
xlabel('Sample points');
ylabel('Current (mA)');

    
    










